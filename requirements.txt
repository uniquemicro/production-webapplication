viivakoodi==0.8.0
reportlab==3.4.0
python3-mysql.connector==1.2.3
git+https://github.com/dpallot/simple-websocket-server.git
Pillow==5.0.0