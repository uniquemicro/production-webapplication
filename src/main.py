import threading
import asyncio
import json
import os, sys

from Web import Server
# config = dict()
# config["id"] = 1
# config["args"] = None
# config["name"] = "UMD Raspberry Pi SD Card"
# config["description"] = "Flashes new SD card with UMD Raspberry Pi Base Image"
# json.dump( config, open("~/bin/rpi-base-image/config.json", 'w') )

www = Server.Server(port=80, dir="/home/umd/www")
www.start()



#main loop
loop = asyncio.get_event_loop()
try:
    loop.run_forever()
finally:
    www.quit()
    loop.run_until_complete(loop.shutdown_asyncgens())
    loop.close()



# while True:
#     i=1