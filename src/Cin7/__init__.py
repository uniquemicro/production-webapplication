ROOT_URL = "https://api.cin7.com/api/v1/"

USER     = "UniqueMicroAU"
KEY      = "5cecaebfb43a45c681814dab736f36ed"

import requests
import json

def Contacts(
        fields: str=None,
        where: str=None,
        order: str=None,
        page: str=1,
        rows: str=50
    ):

    payload = dict()
    if fields is not None:
        payload["fields"] = fields

    if where is not None:
        payload["where"] = where

    if order is not None:
        payload["order"] = order

    if page is not None:
        payload["page"] = page

    if rows is not None:
        payload["rows"] = rows

    _url = ROOT_URL + "Contacts"
    r = requests.get( _url, params=payload, auth=(USER, KEY) )

    if r.status_code == 200:
        return json.loads( r.text )
    else:
        print( r.request.method, r.url, r.request.headers )
        print ( r.status_code, r.text )
        return {}

def Products(
        fields: str=None,
        where: str=None,
        order: str=None,
        page: str=1,
        rows: str=50
    ):

    payload = dict()
    if fields is not None:
        payload["fields"] = fields

    if where is not None:
        payload["where"] = where

    if order is not None:
        payload["order"] = order

    if page is not None:
        payload["page"] = page

    if rows is not None:
        payload["rows"] = rows

    _url = ROOT_URL + "Products"
    r = requests.get( _url, params=payload, auth=(USER, KEY) )

    if r.status_code == 200:
        return json.loads( r.text )
    else:
        print( r.request.method, r.url, r.request.headers )
        print ( r.status_code, r.text )
        return {}

def ProductOptions(
        fields: str=None,
        where: str=None,
        order: str=None,
        page: str=1,
        rows: str=50
    ):

    payload = dict()
    if fields is not None:
        payload["fields"] = fields

    if where is not None:
        payload["where"] = where

    if order is not None:
        payload["order"] = order

    if page is not None:
        payload["page"] = page

    if rows is not None:
        payload["rows"] = rows

    _url = ROOT_URL + "ProductOptions"
    r = requests.get( _url, params=payload, auth=(USER, KEY) )

    if r.status_code == 200:
        return json.loads( r.text )
    else:
        print( r.request.method, r.url, r.request.headers )
        print ( r.status_code, r.text )
        return {}