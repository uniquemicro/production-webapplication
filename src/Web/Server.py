import json
import re

import time
import datetime
import urllib
import copy

import http.server
import socketserver
import threading
import webbrowser

import os, sys

from Web import API
DEFAULT_PORT = 80

class Server(threading.Thread):
    RETRY_CREATE_ON_FAIL = True
    def __init__(self, port=8080, dir=""):
        super(Server, self).__init__()
        self.HOST = ""
        self.port = port
        self.Handler = ''
        self.httpd = None
        self.dir = dir
        self._quit = False
        self.error = dict()

    def __del__(self):
        self._quit = True;

    def createServer(self, host, port, handler):
        print ( "Creating Server" )
        try:
            server = ThreadedHTTPServer((host, port), handler);
            print("Server created http://localhost:" + str(port))
        except Exception as e:
            if Server.RETRY_CREATE_ON_FAIL:
                server, port = self.createServer(host, port + 1, handler);
            else:
                raise Exception(e)

        server.base_path = self.dir
        return server, port;

    def run(self):
        print ( "Starting Web Server" )
        self.Handler = Handler;
        self.httpd, self.port = self.createServer(self.HOST, self.port, self.Handler)
        self.httpd.server_name = "UMD Production/0.0.1"

        try:
            print("Running")
            self.httpd.serve_forever();
        except:
            exception_name, exception_value = sys.exc_info()[:2]
            self.error["err"] = exception_value;
            self.error["error"] = exception_name;
        finally:
            self.quit()

        self.quit()
        print("WebServer closed")

    def quit(self):
        self._quit = True
        self.httpd.shutdown()
        self.httpd.server_close()


class ThreadedHTTPServer(socketserver.ThreadingMixIn, http.server.HTTPServer):
    """Handle requests in a separate thread."""


class Handler(http.server.SimpleHTTPRequestHandler):
    def translate_path(self, path):
        path = http.server.SimpleHTTPRequestHandler.translate_path(self, path)
        relpath = os.path.relpath(path, os.getcwd())
        fullpath = os.path.join(self.server.base_path, relpath)
        return fullpath

    def end_headers(self):
        self.send_my_headers()
        http.server.SimpleHTTPRequestHandler.end_headers(self)

    def send_my_headers(self):
        self.send_header("Server", self.server.server_name)
        # if not re.search("(.gif|.svg|.png)", self.path):
        #     self.send_header("Cache-Control", "no-cache, no-store, must-revalidate")
        #     self.send_header("Pragma", "no-cache")
        #     self.send_header("Expires", "0")

    def log_message(self, format, *args):
        print("%s - - [%s] %s\n" % (self.address_string(), self.log_date_time_string(), format % args))
        # if self.headers['content-length'] != None:
        #     content_len = int(self.headers['content-length'])
        #     print("\t%s" % ( self.rfile.read(content_len) ) )
        return;

    def useLayout(self, html, status_code=200, message="OK"):
        if ( os.path.isfile( self.translate_path(html) ) ):
            print("Using layout for file '" + html + "'")
            with open( self.translate_path("shared/_Layout.html") ) as l:
                with open( self.translate_path(html) ) as h:
                    el = re.split('<([^<]?)+id="container-body"([^>]?)+><\/([^>]?)+>', l.read())
                    el.insert(1, h.read())
                    compiled_html = ''.join(el).encode(errors='ignore')
                    self.send_response(status_code, message);
                    self.send_header("Content-Length", len(compiled_html));
                    self.send_header("Content-Type", "text/html");
                    self.end_headers();
                    self.wfile.write(compiled_html);
        else:
            http.server.SimpleHTTPRequestHandler.do_GET(self)

    def send_error(self, code, message=None, explain=None):
        if code==400:
            if message==None:
                message="Bad Request"
            self.useLayout( "shared/BadRequest.html", code, message)
        elif code==403:
            if message==None:
                message="Forbidden"
            self.useLayout("shared/403Forbidden.html", code, message)
        elif code==404:
            if message==None:
                message="Not Found"
            self.useLayout("shared/PageNotFound.html", code, message)
        elif code==500:
            if message==None:
                message="Unknown Server Error"
            self.useLayout("shared/Error.html", code, message)
        else:
            if message==None:
                message=str()
            self.useLayout("shared/Error.html", code, message)

    def do_GET(self):
        url = urllib.parse.urlparse(self.path)
        if ( url.path.startswith("/api/") ):
            API.handle_GET(self, self.path)
        elif ( url.path.endswith(".html") ):
            self.useLayout( url.path )
        elif ( url.path.endswith("/") or os.path.isdir(url.path) ):
            self.useLayout( url.path + "index.html" )
        else:
            http.server.SimpleHTTPRequestHandler.do_GET(self)

    def do_POST(self):
        url = urllib.parse.urlparse(self.path)
        if (str(self.path).startswith("/api/")):
            API.handle_POST(self, self.path);
        elif (url.path.endswith(".html")):
            self.useLayout(url.path)
        elif (url.path.endswith("/") or os.path.isdir(url.path)):
            if not url.path.endswith("/"):
                url.path += "/"
            self.useLayout(url.path + "index.html")
        else:
            http.server.SimpleHTTPRequestHandler.do_POST(self)
