import time
import datetime
import urllib
import json, csv

import http.server

import os, copy
import serial.tools.list_ports as seriallist
import traceback

import Process, Print
import UMD.Database


def handle_GET(httpd, path):
    url = urllib.parse.urlparse(path)
    path = path.replace("/api/", "");
    pathSplit = path.split("?");
    p = pathSplit[0].split("/");

    getParams = dict();
    if (len(pathSplit) > 1):
        for param in pathSplit[1].split("&"):
            x = param.split("=");
            if (len(x) == 2):
                if (x[0] != "") and (x[1] != ""):
                    getParams[x[0]] = urllib.parse.unquote(x[1]);

    if (len(p) == 0):
        http.server.SimpleHTTPRequestHandler.do_GET(httpd)
        return;

    if (p[0] == "time"):
        if (p[1] == "get"):
            t = str(time.time() * 1000)
            httpd.send_response(200, "OK");
            httpd.send_header("Content-Length", len(t));
            httpd.send_header("Content-Type", "text/plain");
            httpd.end_headers();
            httpd.wfile.write(t.encode());
            return;
    elif (p[0] == "make"):
        rootdir = "/home/umd/bin/"
        #rootdir="C:/Git/utils/"
        if (p[1] == ''):
            configs = list()
            for dir in os.listdir( rootdir ):
                if os.path.isfile( rootdir + dir + "/config.json" ):
                    configs.append( json.load( open(rootdir + dir + "/config.json", 'r') ) )
                    # print ( configs )

            if configs == list():
                httpd.send_error(code=404, explain="No configs available")
            else:
                resp = json.dumps( configs ).encode()
                httpd.send_response(200, "OK")
                httpd.send_header("Content-Length", len(resp))
                httpd.send_header("Content-Type", "text/plain")
                httpd.end_headers()
                httpd.wfile.write(resp)

        elif ( p[1].isdigit ):
            i=1
            ##GET returns individual config
        else:
            ##not valid request
            httpd.send_error( code=404, explain="Request for all or id not made")

    elif (p[0] == "print"):
        rootdir = "/home/umd/bin/print/"
        #rootdir="C:/Git/utils/"
        if (p[1] == ''):
            configs = list()
            for dir in os.listdir( rootdir ):
                if os.path.isfile( rootdir + dir + "/config.json" ):
                    configs.append( json.load( open(rootdir + dir + "/config.json", 'r') ) )
                    # print ( configs )

            if configs == list():
                httpd.send_error(code=404, explain="No configs available")
            else:
                resp = json.dumps( configs ).encode()
                httpd.send_response(200, "OK")
                httpd.send_header("Content-Length", len(resp))
                httpd.send_header("Content-Type", "text/plain")
                httpd.end_headers()
                httpd.wfile.write(resp)

        elif ( p[1].isdigit ):
            i=1
            ##GET returns individual config
        else:
            ##not valid request
            httpd.send_error( code=404, explain="Request for all or id not made")

    elif (p[0] == "device"):
        if (p[1] == "notify"):
            print("Notify GET recieved")
            # j = dict(json.loads(httpd.rfile.read()))
            #
            # with open("~/devices.log") as f:
            #     f.write(str(datetime.datetime.now()) + ",localAddress=" + j["localAddress"] + ",partNumber=" + j[
            #         "partNumber"])

            httpd.send_response(200, "OK")
            httpd.end_headers()

    elif (p[0] == "server"):
        if (p[1] == "serialports"):
            l = seriallist.comports()
            l2 = list()
            for port in l:
                d = dict()
                d["device"] = port.device
                d["name"] = port.name
                d["description"] = port.description
                d["hwid"] = port.hwid
                d["vendorId"] = port.vid
                d["productId"] = port.pid
                d["vendor"] = port.manufacturer
                d["product"] = port.product
                l2.append( d )

            resp = json.dumps(l2).encode(errors="BACKSLASHREPLACE")

            httpd.send_response(200, "OK")
            httpd.send_header("Content-Length", len(resp))
            httpd.end_headers()
            httpd.wfile.write(resp)
            return

    elif (p[0] == "config"):
        hndl = None
        try:
            # print ( url.path )
            # print ( repr(getParams) )
            query = "SELECT * FROM `" + p[1] + "` WHERE ("
            queryWhere = list()
            for key in getParams.keys():
                where = "`" + key + "`="
                if key in ("part"):
                    where += str(int(getParams[key]))
                else:
                    where += "'" + str(getParams[key]) + "'"
                queryWhere.append( where )
            query += ' AND '.join(queryWhere) + ")"
            # print ( query )
            cnx = UMD.Database.open()
            hndl = cnx.cursor()
            # print ( "Executing" )
            hndl.execute(query)

            result = list()
            columns = tuple([d[0] for d in hndl.description])
            for row in hndl:
                result.append(dict(zip(columns, row)))
            # print( repr(result) )
            resp = json.dumps( result ).encode(errors='BACKSLASHREPLACE')

            httpd.send_response(200, "OK")
            httpd.send_header("Content-Length", len(resp))
            httpd.end_headers()
            httpd.wfile.write(resp)
        except:
            httpd.send_response(500, "Internal Server Error")
            httpd.end_headers()

        finally:

            if hndl != None:
                hndl.close()
            cnx.close()


        return
    elif (p[0] == "jobQueue"):
        # print ( url )
        hndl = None
        try:
            # print( "get jobQueue for " + str(p[1]).upper() )
            query = "SELECT * FROM `jobQueue` WHERE `type`='" + str(p[1]).upper() + "' AND `expiry`>NOW() ORDER BY `id` ASC"
            cnx = UMD.Database.open()
            hndl = cnx.cursor()
            # print (query)
            hndl.execute(query)

            result = list()
            columns = tuple([d[0] for d in hndl.description])
            for row in hndl:
                item = dict(zip(columns, row))
                item["created"] = str(item["created"])
                item["expiry"] = str(item["expiry"])
                result.append(item)
            # print( repr(result) )
            resp = json.dumps( result )

            # if port name in com ports, and in argv, replace
            for port in seriallist.comports():
                if port.description in resp:
                    resp = resp.replace(str("com='" + port.description + "'"), port.device)

            resp = resp.encode(errors='BACKSLASHREPLACE')

            httpd.send_response(200, "OK")
            httpd.send_header("Content-Length", len(resp))
            httpd.end_headers()
            httpd.wfile.write(resp)
        except:
            httpd.send_response(500, "Internal Server Error")
            httpd.end_headers()
            print ( "Unknown exception" )
            print ( traceback.format_exc() )
        finally:
            try:
                if hndl is not None:
                    hndl.close()
                cnx.close()
            except:
                pass

        return

def handle_POST(httpd, path):
    url = urllib.parse.urlparse(path)
    path = path.replace("/api/", "");
    pathSplit = path.split("?");
    p = pathSplit[0].split("/");

    getParams = dict();
    if (len(pathSplit) > 1):
        for param in pathSplit[1].split("&"):
            x = param.split("=");
            if (len(x) == 2):
                if (x[0] != "") and (x[1] != ""):
                    getParams[x[0]] = urllib.parse.unquote(x[1]);

    if (len(p) == 0):
        http.server.SimpleHTTPRequestHandler.do_GET(httpd)
        return;

    if (p[0] == "make"):
        rootdir = "/home/umd/bin/"
        #rootdir="C:/Git/utils/"
        if (p[1] == ''):
            return httpd.send_error(code=404, explain="Can't make all, use a ID")

        elif (p[1].isdigit):
            config = dict()
            binDir = ""
            for dir in os.listdir( rootdir ):
                if os.path.isfile( rootdir + dir + "/config.json" ):
                    temp = json.load( open(rootdir + dir + "/config.json", 'r') )
                    # print ( temp )
                    if ( int(temp["id"]) == int(p[1]) ):
                        config = temp
                        binDir = rootdir + dir
                        break

            if config == dict():
                httpd.send_error(code=404, explain="No configs available")
            else:
                #good to go, end ws:// link for watching of stdout of process
                #must initalise in main loop, on callback do response
                argv = list()
                if "argv" in getParams:
                    argv = getParams["argv"].split(" ")

                print( "argv==" + repr(argv) )

                m = Process.Make( binDir, argv )
                m.start()
                print( "Process.Make started with port==" + str(m.port) )

                if m.port is not None:
                    resp = json.dumps( {"stdout": m.port} ).encode(errors='replace')

                    httpd.send_response(200, "OK")
                    httpd.send_header("Content-Length", len(resp))
                    httpd.send_header("Content-Type", "application/json")
                    httpd.end_headers()
                    httpd.wfile.write( resp )
                else:
                    httpd.send_error(code=500, explain="Could not create websocket for stdout")
        else:
            ##not valid request
            httpd.send_error( code=404, explain="Request for all or id not made")

    elif (p[0] == "print"):
        print( "Print API called" )
        rootdir = "/home/umd/bin/print/"
        #rootdir="C:/Git/utils/"
        if (p[1] == ''):
            return httpd.send_error(code=404, explain="Can't make all, use a ID")

        elif (p[1].isdigit):
            print("Get Config")
            config = dict()
            binDir = ""
            for dir in os.listdir( rootdir ):
                if os.path.isfile( rootdir + dir + "/config.json" ):
                    temp = json.load( open(rootdir + dir + "/config.json", 'r') )
                    # print ( temp )
                    if ( int(temp["id"]) == int(p[1]) ):
                        config = temp
                        binDir = rootdir + dir
                        break

            if config == dict():
                httpd.send_error(code=404, explain="No configs available")
            else:
                try:
                    argv = list()
                    if "argv" in getParams:
                        argv = getParams["argv"].split(" ")
                    ## argv[0] == sales order number
                    ## argv[1] == customerVariant->id

                    order = argv[0]

                    if len(argv)==0:
                        return httpd.send_error(code=400, explain="No params given")

                    print( "Opening database" )
                    cnx = UMD.Database.open()
                    hndl = cnx.cursor()
                    try:

                        ## download csv for use
                        print( "Load POST body" )
                        content_len = int(httpd.headers['content-length'])
                        c = httpd.rfile.read( content_len ).decode(errors="BACKSLASHREPLACE")
                        csvFile = "/tmp/print_"+ str( int(time.time()*1000000) ) + ".csv"
                        with open(csvFile, 'w+') as f:
                            f.write( c )
                        ## CSV now available at 'csvFile'


                        ## prepare locations for use
                        print( "Load locations file" )
                        locationsFile = "/tmp/locations_" + str(int(time.time() * 1000000)) + ".csv"
                        query = "SELECT `value` FROM `productionVariant` WHERE id=" + str(int(p[1])) + " LIMIT 1"
                        # print (query)
                        # print ( "Executing" )
                        hndl.execute(query)
                        for row in hndl:
                            with open(locationsFile, 'w+') as f:
                                f.write(row[0].replace("\\n", "\n"))
                        ## location now available for use at 'locationsFile'

                        ## prepare printObjects for use
                        printObjectsFile = "/tmp/printObjects_" + str(int(time.time() * 1000000)) + ".csv"
                        query = "SELECT `value` FROM `customerVariant` WHERE id=" + str(int(argv[1])) + " LIMIT 1"
                        # print (query)
                        hndl.execute(query)
                        for row in hndl:
                            with open(printObjectsFile, 'w+') as f:
                                f.write(row[0].replace("\\n", "\n"))
                        ## printObjects now available for use at 'printObjectsFile'

                        ## close database connection during PDF creation
                        hndl.close()
                        cnx.close()

                        objects = None
                        test = Print.Serialise(locationsFile, printObjectsFile, objects)
                        test.load_serials_from_file( csvFile )
                        pageTemplate = "/tmp/create_"+ str( int(time.time()*1000000) ) + ".pdf"
                        fileList = test.save(pageTemplate)

                        dat = dict()
                        dat["count"] = len(fileList)
                        dat["files"] = list()

                        ## open connection again for new use.
                        cnx = UMD.Database.open()
                        hndl = cnx.cursor()

                        ## Save remaining pages to queue
                        if ( len(fileList)>0 ):
                            print( "Save remaining PDFs in queue, and move to webserver" )
                            query = "INSERT INTO `jobQueue` (`part`, `order`, `type`, `expiry`, `description`, `value`) VALUES ("
                            l = list()
                            count=1
                            for tray in fileList:
                                newName = "/tmp/engrave_"+str(argv[0])+"_"+str(count)+".pdf"
                                dat["files"].append( newName )
                                os.rename( tray, "/home/umd/www" + newName )
                                l.append(
                                    p[1] + ", " +
                                    "'" + argv[0] + "', " +
                                    "'ENGRAVE', " +
                                    "DATE_ADD(NOW(), INTERVAL 2 DAY), " +
                                    "'" + config["name"] + ", tray #" + str(count) + "', " +
                                    "'" + newName + "'"
                                )
                                count += 1
                            query += '), ('.join(l)
                            query += ")"
                            hndl.execute(query)

                        resp = json.dumps(dat).encode(errors="BACKSLASHREPLACE")

                        httpd.send_response(200, "OK")
                        httpd.send_header("Content-Length", len(resp))
                        httpd.send_header("Content-Type", "application/json")
                        httpd.end_headers()
                        httpd.wfile.write(resp)

                        ## Create linked jobs if required
                        itemsPerTray = config["maxItems"]
                        itemCount = 1

                        with open(csvFile, 'r') as f:
                            serialReader = csv.DictReader(f);
                            itemsRemain = True
                            tray = 0
                            while itemsRemain:
                                tray += 1
                                for job in config["linkedJobs"]:
                                    # print( repr(job) )
                                    if job["type"]=="make":
                                        args = list()
                                        for arg in job["args"]:
                                            if arg == "order":
                                                args.append(order)
                                            elif arg.startswith("<file>"):
                                                arg = arg.replace("<file>", "")
                                                arg = arg.replace("'", "")
                                                enteringLoop = True
                                                while enteringLoop or itemCount%32 > 0:
                                                    enteringLoop = False
                                                    try:
                                                        row = next(serialReader)
                                                        # print( arg + "->" + arg.format(**row) )
                                                        args.append( str(arg.format(**row)) )
                                                        itemCount += 1
                                                    except StopIteration as e:
                                                        # print( "end of iterator, stopping" )
                                                        itemsRemain = False
                                                        break
                                            else:
                                                arg = arg.replace("'", "''")
                                                args.append(arg)
                                        query = "INSERT INTO `jobQueue` (`part`, `order`, `type`, `expiry`, `description`, `value`) VALUES ( " + str(job["id"]) + ", '" + str(order) + "', '" + str(job["type"]).upper() + "', " + "DATE_ADD(NOW(), INTERVAL 2 DAY), " + "'Tray #" + str(tray) + " from " + config["name"] + "', '" + (" ".join(args)) + "')"
                                        hndl.execute(query)
                                        # print( query )

                        os.remove(fileList[0])
                        os.remove(printObjectsFile)
                        os.remove(locationsFile)
                        os.remove(csvFile)

                        return
                    except:

                        print("Unexpected Error, here is trace")
                        print(traceback.format_exc())
                        httpd.send_error(code=500, explain="Internal Server Error")
                    finally:
                        try:
                            hndl.close()
                            cnx.close()
                        except:
                            pass

                except:
                    httpd.send_error(code=500, explain="Unexpected error")
                    print( "Unexpected Error, here is trace" )
                    print( traceback.format_exc() )
        else:
            ##not valid request
            httpd.send_error( code=404, explain="Request for all or id not made")
    elif (p[0] == "device"):
        if (p[1] == "notify"):
            print ("Notify recieved")

            content_len = int(httpd.headers['content-length'])
            j = dict( json.loads( bytes(httpd.rfile.read(content_len)).decode(errors='REPLACE') ) )

            with open("/home/umd/www/log/devices.csv", 'a') as f:
                f.write( datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+","+j["localAddress"]+","+j["partNumber"]+","+j["notes"]+"\n" )

            resp = "OK".encode()
            httpd.send_response(200, "OK")
            httpd.send_header("Content-Length", len(resp))
            httpd.end_headers()
            httpd.wfile.write(resp)
            print("End notify recieved")
            return

