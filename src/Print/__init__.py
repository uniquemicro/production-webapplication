import threading
import webbrowser
import traceback
from reportlab.pdfgen import canvas, textobject
from reportlab.lib.units import inch, cm, mm
from reportlab.graphics.shapes import Drawing
from reportlab.platypus import Flowable
# from svglib.svglib import svg2rlg
import barcode, qrcode
from barcode.writer import ImageWriter
import qrcode.image.svg
from enum import Enum
import sys, os
import csv

SIZE_LASERCUTTER_X = 457*mm
SIZE_LASERCUTTER_Y = 305*mm

class Type( Enum ):
    TEXT    = 0
    SERIAL  = 1
    IMAGE   = 2
    BARCODE = 3
    QR_CODE = 4

class Object( object ):
    def __init__(self):
        self.type = Type['TEXT']
        self.item = str()
        self.x = 0
        self.y = 0
        self.properties = { "font-size": "12" }

class Serialise( threading.Thread ):
    def __init__(self,
                 locations: str,
                 printObjects: str,
                 items: list=None
                 ):
        super(Serialise, self).__init__()
        self.items = items
        self.locations = None
        self.printObjects = dict()
        self.objectSize = None
        self.containsSerial = set()
        self.isTestPrint = False
        self.serialNumbers = None
        self.printPagesAsSeparateDocuments = True

        with open(locations, "r") as l:
            line0 = l.readline().split(",")
            max_x = int(line0[0])
            max_y = int(line0[1])
            self.locations = [[None]*max_y for _ in range(max_x)]
            self.objectSize = ( float(line0[2])*mm, float(line0[3])*mm )
            self.objectSize = ( float(line0[2])*mm, float(line0[3])*mm )
            for line in l:
                try:
                    # print ( line )
                    item = line.replace("\n","").replace("\r","").split(",")
                    self.locations[ int(item[0]) ][ int(item[1]) ] = (float(item[2])*mm, float(item[3])*mm)
                except:
                    print( "Could not parse line in locations, locationsDir==" + repr(locations) + ", line==" + repr(line))
                    print( traceback.format_exc() )
                    continue

            if self.items == None:
                self.items = [(x, y) for y in range(max_y) for x in range(max_x)]
                #print( self.items )

        with open(printObjects, 'r') as o:
            i = 0
            reader = csv.reader( o )
            for item in reader:
                try:
                    if "," not in line:
                        continue

                    obj = Object()
                    obj.type = Type[item[2]]
                    obj.item = item[3]
                    props = dict()
                    if len(item)>4:
                        for prop in item[4].split(";"):
                            if "=" in prop:
                                x = prop.split("=")
                                props[ x[0] ]=x[1]
                    obj.properties = props
                    obj.x = float(item[0])*mm
                    obj.y = float(item[1])*mm

                    self.printObjects[i] = obj

                    if obj.type==Type.SERIAL or obj.type==Type.BARCODE or obj.type==Type.QR_CODE:
                        self.containsSerial.add( obj.item )

                    i += 1

                except:
                    print(
                        "Could not parse line in locations, locationsDir==" + repr(locations) + ", line==" + repr(line))
                    print(traceback.format_exc())
                    continue

    def load_serials_from_file(self, file):
        with open( file, 'r' ) as f:
            serialReader = csv.DictReader(f);
            self.serialNumbers = dict()
            self.serialNumbers["VISUAL"] = list()
            for row in serialReader:
                self.serialNumbers["VISUAL"].append( row["VISUAL"] )
            print ( repr(self.serialNumbers) )


    def request_serials(self, products, count):
        serials = dict()
        for product in products:
            try:
                serialFile = "C:\Git\production-webapplication\src\Print\example\lastSerial_" + product + ".txt"
                try:
                    s = 0
                    with open(serialFile, 'r') as f:
                        s = int( f.readline().replace("\n", "").replace("\r", "") )

                    with open(serialFile, 'w') as f:
                        f.write( str(s+count) )

                    serials[product] = list(range(s, s + count))
                except FileNotFoundError as e:
                    with open(serialFile, 'w+') as f:
                        f.write( str(count) )

                    serials[product] = list(range(1, count+1))
            except:
                print( "Could not request serialFile, product==" + repr(product) )
                print( traceback.format_exc() )
                serials[product] = None
        return serials


    def save(self, filePath: str):
        filePathTemplate = filePath
        if self.printPagesAsSeparateDocuments:
            filePathTemplate = filePath.replace(".pdf", "_page%d.pdf")

        filePathList = list()
        pageCount = 1
        serials = self.serialNumbers
        if len(self.containsSerial)>0:
            if serials == None:
                serials = self.request_serials( self.containsSerial, len(self.items))

        if self.printPagesAsSeparateDocuments:
            fn = filePathTemplate % pageCount
        else:
            fn = filePathTemplate

        print ( "Creating canvas, fileName=" + fn )
        c = canvas.Canvas(
            fn,
            pagesize=( SIZE_LASERCUTTER_X, SIZE_LASERCUTTER_Y )
        )
        #print( repr(self.locations) )
        i = 0
        while len(serials)>0:
            for coord in self.items:

                if len(serials[ list(serials.keys())[0] ])==0:
                    serials.clear()
                    break

                try:
                    x_n, y_n = coord
                    #print(coord)
                    objToPrint_coords = self.locations[ x_n ][ y_n ]
                    #print(repr(objToPrint_coords))
                    x0 = objToPrint_coords[0]
                    y0 = SIZE_LASERCUTTER_Y - objToPrint_coords[1]
                    for j in self.printObjects:
                        obj = self.printObjects[j]

                        size = 12
                        if "font-size" in obj.properties:
                            size = (float(obj.properties["font-size"]))

                        align = "left"
                        if "align" in obj.properties:
                            align = str(obj.properties["align"])

                        width = 0  # fill space required
                        if "width" in obj.properties:
                            width = float(obj.properties["width"]) * mm

                        height = 0  # fill space required
                        if "height" in obj.properties:
                            height = float(obj.properties["height"]) * mm

                        rotate = 0
                        if "rotate" in obj.properties:
                            rotate = float(obj.properties["rotate"])

                        symb = "code128"
                        try:
                            if "symbology" in obj.properties:
                                if str(obj.properties["symbology"]) in barcode.PROVIDED_BARCODES:
                                    symb = str(obj.properties["symbology"])
                            symb = barcode.get_barcode_class(symb)
                        except:
                            pass

                        serial = str()
                        try:
                            serial = serials[obj.item][0]
                        except KeyError as e:
                            serial = "ERR_KEY"

                        if obj.type == Type.TEXT:
                            align = "left"
                            width = 0  # fill space required
                            size = (float(obj.properties["font-size"]) if "font-size" in obj.properties else 12)
                            text = obj.item

                            if "format" in obj.properties:
                                serial = (obj.properties["format"]).format(text)
                            if "align" in obj.properties:
                                align = str(obj.properties["align"])
                            if "width" in obj.properties:
                                width = float(obj.properties["width"]) * mm

                            if "format" in obj.properties:
                                text = (obj.properties["format"]).format(text)

                            c.setFont("Courier", size)
                            c.saveState()

                            if align == "left":
                                c.translate(x0 + obj.x, y0 - obj.y - size)
                                c.rotate(rotate)
                                c.drawString(0, 0, text)
                            elif align == "right":
                                c.translate(x0 + obj.x + width, y0 - obj.y - size)
                                c.rotate(rotate)
                                c.drawRightString(0, 0, text)
                            elif align == "center":
                                c.translate(x0 + obj.x + 0.5 * width, y0 - obj.y - size)
                                c.rotate(rotate)
                                c.drawCentredString(0, 0, text)
                            else:
                                c.translate(x0 + obj.x, y0 - obj.y - size)
                                c.rotate(rotate)
                                c.drawString(x0 + obj.x, y0 - obj.y - size, text)

                            c.restoreState()

                        elif obj.type == Type.IMAGE:
                            c.saveState()
                            c.translate(x0 + obj.x, y0 - obj.y - height)
                            c.rotate( rotate )
                            c.drawImage( "/home/umd/" + obj.item, 0, 0, width, height)
                            c.restoreState()

                        elif obj.type == Type.SERIAL:
                            try:
                                if "format" in obj.properties:
                                    serial = (obj.properties["format"]).format(serial)
                            except:
                                serial = "ERR_FMT"

                            c.setFont("Courier", size)
                            c.saveState()

                            if align=="left":
                                c.translate(x0 + obj.x, y0 - obj.y - size)
                                c.rotate( rotate )
                                c.drawString(0, 0, str(serial))
                            elif align=="right":
                                c.translate(x0 + obj.x + width, y0 - obj.y - size)
                                c.rotate(rotate)
                                c.drawRightString(0, 0, str(serial))
                            elif align=="center":
                                c.translate(x0 + obj.x + 0.5 * width, y0 - obj.y - size)
                                c.rotate(rotate)
                                c.drawCentredString(0, 0, str(serial))
                            else:
                                c.translate(x0 + obj.x, y0 - obj.y - size)
                                c.rotate(rotate)
                                c.drawString(x0 + obj.x, y0 - obj.y - size, str(serial))

                            c.restoreState()

                        elif obj.type == Type.BARCODE:
                            if "format" in obj.properties:
                                serial = (obj.properties["format"]).format(serial)

                            thisBarcodeFileLocation = os.getcwd() + "\\barcode_"+str(serial)
                            options = dict(write_text=False)
                            symb( serial,
                                  writer=ImageWriter()
                            ).save( thisBarcodeFileLocation, options )
                            thisBarcodeFileLocation += ".png"
                            c.drawImage(thisBarcodeFileLocation, x0 + obj.x, y0 - obj.y - height, width, height)
                            os.remove(thisBarcodeFileLocation)
                        elif obj.type == Type.QR_CODE:
                            if "format" in obj.properties:
                                serial = (obj.properties["format"]).format(serial)

                            thisQrFileLocation = os.getcwd() + "\\qr_" + str(serial) + ".png"
                            qr = qrcode.QRCode(
                                version=1,
                                error_correction=qrcode.constants.ERROR_CORRECT_L,
                                box_size=9,
                                border=1,
                            )
                            qr.add_data(serial)
                            qr.make(fit=True)
                            img = qr.make_image(
                                          fill_color="black",
                                          back_color="white"
                                          )
                            img.save(thisQrFileLocation)
                            #
                            # drawing = svg2rlg(thisQrFileLocation)
                            # c.drawPath( drawing )

                            c.drawImage(thisQrFileLocation, x0 + obj.x, y0 - obj.y - height, width, height)

                            os.remove(thisQrFileLocation)

                    if self.isTestPrint:
                        outlineObj = c.beginPath()
                        outlineObj.rect(x0, y0-self.objectSize[1], self.objectSize[0], self.objectSize[1])
                        c.drawPath( outlineObj, stroke=1)
                    i += 1
                except:
                    print( "Unhandled exception" )
                    print ( traceback.format_exc() )
                finally:
                    for key in serials:
                        if len(serials[key])>0:
                            serials[key].pop(0)
            c.showPage()
            if self.printPagesAsSeparateDocuments:
                print( "Printing more than one page, separting" )
                c.save()
                filePathList.append( str(c._filename) )
                pageCount += 1
                c = canvas.Canvas(
                    filePathTemplate % pageCount,
                    pagesize=(SIZE_LASERCUTTER_X, SIZE_LASERCUTTER_Y)
                )
                print("New path, c.fileName==" + c._filename)
                pass

        if not self.printPagesAsSeparateDocuments:
            c.save()
            filePathList += filePath

        return filePathList



if __name__ == '__main__':
    locations       = "C:\Git\production-webapplication\src\Print\example\locations.csv"
    printObjects    = "C:\Git\production-webapplication\src\Print\example\printObjects.csv"
    serials         = "C:\Git\production-webapplication\src\Print\example\serials.csv"
    objects         = None #do all objects

    test = Serialise( locations, printObjects, objects )
    test.load_serials_from_file(serials)
    test.isTestPrint = True
    test.printPagesAsSeparateDocuments = False
    test.save( "C:\Git\production-webapplication\src\Print\example\create.pdf" )

    webbrowser.open("C:\Git\production-webapplication\src\Print\example\create.pdf")