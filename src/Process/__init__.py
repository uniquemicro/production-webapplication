#!/usr/bin/env python

from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket
import threading
import time
import subprocess
import copy

class PipeReader( threading.Thread ):
    def __init__(self, pipe):
        super(PipeReader, self).__init__()
        self.pipe = pipe
        self._read = None
        self.lock = threading.Lock()
        self.start()


    def run(self):
        while (self.pipe.readable() if self.pipe else True):
            try:
                # print ("PipeReader.run(), loop")
                r = self.pipe.readline()
                if self._read == None:
                    self._read = str()
                with self.lock:
                    self._read += r
                # print("PipeReader.run(), recv=='" + r + "'")
            finally:
                time.sleep(0.1)
        print ("PipeReader.run(), end")

    def read(self):
        with self.lock:
            txt = copy.deepcopy( self._read )
            self._read = None
            return txt

class Make( threading.Thread ):

    class Do( threading.Thread ):
        def __init__(self, binDir, argv, on_finish, on_newline, port):
            super(Make.Do, self).__init__()
            self.binDir = binDir
            self.on_newline = on_newline
            self.on_finish = on_finish
            self.argv = argv
            self.partNumber = str()
            self.serial = str()
            self.config = dict()
            self.handle = None
            self.stdout = None
            self.stderr = None
            self.port = port
            self.stdin = None
            # self.stdin = open("/tmp/stdin_"+str(port), "a")

        def logToProductionDatabase(self, result):
            # query = "INSERT INTO `productionLog` " \
            #         "(`datetime`, `partNumber`, `serial`, `productionJob`, `parameters`, `comments`) " \
            #         "VALUES ( NOW(),  " \
            #         "'" + (self.config["partNumber"] if "partNumber" in self.config else str()) + "'," \
            #         "[value-3],[value-4],[value-5],[value-6],[value-7])
            pass

        def message_recv(self, msg):
            self.stdin.write(msg)
            self.stdin.flush()
            print( "Make.Do.message_recv(), msg=='" + msg + "'" )

        def run(self):
            print("Make.Do.run()")
            cmd = ['bash', str(self.binDir + "/make.sh")]
            if len(self.argv) > 0:
                for arg in self.argv:
                    cmd.append( arg )

            self.handle = subprocess.Popen(cmd,
                                      universal_newlines=True,
                                      stdout=subprocess.PIPE,
                                      stderr=subprocess.PIPE,
                                      stdin=subprocess.PIPE)
            text = str()
            self.stdout = PipeReader(self.handle.stdout)
            self.stderr = PipeReader(self.handle.stderr)
            self.stdin = self.handle.stdin
            print ( "Begin polling handle" )
            while self.handle.poll() == None:
                # print("Make.Do.run(), loop")
                time.sleep(0.1)

                if self.stdout:
                    text = self.stdout.read()
                    if text is not None:
                        # print ( text )
                        self.on_newline( text )

                if self.stderr:
                    text = self.stderr.read()
                    if text is not None:
                        # print ( text )
                        self.on_newline( text )

            print ( "Make.Do.run() returning" )

            try:
                #read again for one final time after break
                text = self.stdout.read()
                if text is not None:
                    print(text)
                    self.on_newline(text)

                text = self.stderr.read()
                if text is not None:
                    print(text)
                    self.on_newline(text)
            finally:
                result = self.handle.poll()
                self.logToProductionDatabase( result );

                if ( result ):
                    self.on_newline("Success!")
                else:
                    self.on_newline("Complete with error code " + str(self.handle.poll()) + "\n")
                self.on_finish()


    class Handler(WebSocket):
        def handleConnected(self):
            print( "Handling connection" )
            self.sendMessage("The programs included with the Debian GNU/Linux system are free software; "
                             "the exact distribution terms for each program are described in the "
                             "individual files in /usr/share/doc/*/copyright. Debian GNU/Linux comes "
                             "with ABSOLUTELY NO WARRANTY, to the extent permitted by applicable law.\n")
            self.sendMessage("\n")
            self.sendMessage("UMD Provisioning Service\n")
            self.sendMessage("\n")
            if not self.server.running_script:
                self.server.running_script = True
                try:
                    print( "self.server.argv==" + repr(self.server.argv) )
                    cmd = "bash " + self.server.script_dir + "/make.sh" + str(" ").format(self.server.argv)
                    self.sendMessage("provisioning" + str(self.server.port) + "@raspi-prod-01:~ $ " + cmd + "\n\n")
                    self.server.do = Make.Do( self.server.script_dir,
                                  argv=self.server.argv,
                                  on_finish=self.close,
                                  on_newline=self.sendMessage,
                                  port=self.server.port )
                    self.server.do.start()
                    self.sendMessage("\n")
                except Exception as e:
                    print( e.__repr__() )
                    self.sendMessage( e.__repr__() )
                    self.close()
            else:
                self.sendMessage("Window already opened")
                self.close()
                raise OSError("Already running script")
            print("End connection routine")

        def handleClose(self):
            print(self.address, 'closed')
            self.server.do.handle.terminate()

        def handleMessage(self):
            # echo message back to client
            self.server.do.message_recv(self.data)

    def __init__(self, script_dir, argv):
        super(Make, self).__init__()
        self.script_dir     = script_dir
        self.argv           = argv
        self.port           = 8765
        self.running_script = False
        self.server_handle  = self.create_server()

    def create_server(self):
        ws = None

        try:
            ws = SimpleWebSocketServer('', self.port, Make.Handler)
        except OSError as e:
            self.port += 1
            ws = self.create_server()

        ws.running_script = False
        ws.script_dir = self.script_dir
        ws.argv = self.argv
        ws.port = self.port
        return ws

    def run(self):
        print ( "Run Server ")
        if self.server_handle != None:
            self.server_handle.serveforever()
        else:
            raise Exception("Server not created")



