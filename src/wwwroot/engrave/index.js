var url = new URL(window.location.href);
var socket;
var POST_body = "";

$(document).on("DOMSubtreeModified propertychange", ".box", function(e) {
    $(this).animate({ scrollTop: $(this).prop("scrollHeight") }, 100);
});

var argv = Array();

function setArg( id, arg, val ){
    console.log( id, arg, val );
    argv[id][arg] = val;
}

window.onload = function(){

    load_jobQueue();

    $.getJSON('/api/print/', function( data ){
        var table = document.getElementById("tabPrintList");
        for ( var i in data ){
            var item = data[i];
            var tr = document.createElement('tr');
            tr.className = "table-hover";

            var td = document.createElement('td');
            td.innerHTML = item.name;
            tr.appendChild( td );

            var td = document.createElement('td');
            td.innerHTML = item.description;
            tr.appendChild( td );

            var td = document.createElement('td');
            td.innerHTML = '<button id="btnPrint'+ item.id + '" type="button" class="btn btn-info btn-med" data-toggle="modal" data-target="#modalOnPrint' + item.id + '">' +
                '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>' +
                ' Print</button>';
            tr.appendChild( td );

            table.appendChild( tr );

            var btn = document.getElementById("btnPrint" + item.id);
            btn.id = item.id;
            btn.args = item.args;
            btn.item = item;
            btn.POST_body = "";
            if ( item.args ) {
                argv[item.id] = Array(item.args.length);

                btn.onclick = function () {

                    if (document.getElementById("modalOnPrint" + this.id)) {
                        console.log("Modal exists, opening");
                        //open only, as already exisits
                    } else {
                        fileCount = 1;

                        //does not exist, create modal and open
                        divModal = document.createElement('div');
                        divModal.id = "modalOnPrint" + this.id;
                        divModal.className = "modal fade";
                        divModal.role = "dialog";
                        var html = '<div class="modal-dialog">' +
                            '<!-- Modal content-->' +
                            '<div class="modal-content">' +
                            '<div class="modal-header">' +
                            '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                            '<h4 class="modal-title">Configuration</h4>' +
                            '</div>' +
                            '<div class="modal-body">' +
                            '<p>Enter additional configuration values:</p>' +
                            '<table width="100%">';
                        for (var i = 0; i < this.args.length; i++) {
                            var item = this.args[i];
                            html += '<tr>';
                            html += '<td width="15%">' + item.name + ':</td>';
                            html += '<td>';
                            if ( !item.type ){
                                html += '<input type="text" id="inpMake' + this.id + 'Arg' + i + '" onchange="setArg(' + this.id + ',' + i + ',this.value)" /></td>';
                            } else if ( item.type == "config_printObjects" ) {
                                _id = 'inpMake' + this.id + 'Arg' + i;
                                var id = this.id;
                                var arg = i;
                                html += '<select id="inpMake' + this.id + 'Arg' + i + '" style="width: 100%" onchange="setArg(' + this.id + ',' + i + ',this.value)"></select>';
                                $.getJSON('/api/config/customerVariant?type=ENGRAVE&key=printObjects&part=' + this.item.productId, function(data){
                                    console.log(this._id);
                                    sel = document.getElementById( _id );
                                    data.forEach( function(currentValue, index, array){
                                        opt = document.createElement('option');
                                        if ( currentValue.customer ) {
                                            if (currentValue.customer > 0) {
                                                opt.innerHTML = "[Cust#" + currentValue.customer + "] ";
                                            }
                                        }
                                        if ( currentValue.description ){
                                            opt.innerHTML += currentValue.description;
                                        }
                                        opt.value = currentValue.id;
                                        sel.append( opt );
                                    });
                                    setArg(id, arg, sel.value);
                                });
                            } else if ( item.type == "file" ){
                                html += '<input type="file" ' +
                                    'name="file' + fileCount + '" ' +
                                    'id="inpPrint' + this.id + 'Arg' + i + '" ' +
                                    'onchange="readFile(this, function(str){ POST_body = str; })"';
                                html += ' />';
                                if ( item.example ) {
                                    html += 'Example file: <a href="' + item.example + '">Download</a></td>';
                                }
                                fileCount += 1;
                            }
                            html += '</tr>';
                            html += '<tr>';
                            html += '<td colSpan="2" style="font-size: smaller; font-style: italic; padding-bottom: 12px;">' + item.description + '</td>';
                            html += '</tr>'
                        }
                        html += '</table>' +
                            '</div>' +
                            '<div class="modal-footer">' +
                            '<button style="background-color: indianred" type="button" class="btn btn-default" data-dismiss="modal">&times; Close</button>' +
                            '<button id="btnDoMake' + this.id + '" style="background-color: greenyellow" onclick="do_print(' + this.id + ', POST_body)" type="button" class="btn btn-default" data-dismiss="modal" data-target="#modalOnWait" ><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> Print</button>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                        divModal.innerHTML = html;


                        document.body.appendChild(divModal);

                        var id_focus = 'inpPrint' + this.id + 'Arg0';
                        window.setTimeout(function () {
                            try {
                                document.getElementById(id_focus).focus().val("");
                            } catch (err) {};
                        }, 1000);
                        this.setAttribute("data-toggle", "modal");
                        this.setAttribute("modalMake", this.id);
                        this.addEventListener("keypress", function (event) {
                            event.preventDefault();
                            if (event.keyCode === 13) { //on enter key
                                document.getElementById("btnDoPrint" + this.id).click();
                            }
                        });
                    }
                }
            } else {
                argv[item.id] = Array(0);
                btn.onclick = function () {
                    do_print( this.id, this.POST_body );
                }
            }
        }

        document.getElementById("imgLoading").style.display = 'none';

    });

};

function do_print( id, post_body ){
    if ( id === 'undefined' )
        throw "ID not given";

    var a = argv[id].join(" ");

    if ( post_body != "" ){
        show_waitScreen( true );
        $.ajax({
            type: "POST",
            url: '/api/print/' + id + ((a.length>(argv[id].length-1))?("?argv=" + a):""),
            data: post_body,
            success: function( data ){

                console.log( data );

                var body = document.getElementById("divModalOnWaitBody");
                body.innerHTML = "" +
                    "<p>Done.</p>";

                if ( data.count > 0 ) {
                    body.innerHTML += "" +
                        "<p>All trays have been added to the job queue, and can be accessed for 2 days. This window can be closed and documents accessed from Engrave</p>" +
                        "<p>Tray Count: " + (data.count) + "</p>"

                    for ( var idx in data.files ){
                        if ( idx > 10 ) {
                            body.innerHTML += "<p>Others available in job queue...</p>";
                            break;
                        }

                        body.innerHTML += "<p><a href='" + data.files[idx] + "' target='_blank' ><img src='/img/pdf.png' style='width: 16px' /></a> Tray #" + (Number(idx)+1) + "</p>"
                    }

                } else {
                    body.innerHTML += "<p>No documents have been generated</p>";
                }

                body.innerHTML += "<button type=\"button\" class=\"btn btn-info btn-med\" data-dismiss=\"modal\">&nbsp;&times;&nbsp;Close</button>";
            },
            dataType: "json"
        }).fail(
            function(){
                document.getElementById("divModalOnWaitBody").innerHTML = "" +
                    "<p>An error occured while generating the Print/Engrave documents</p>" +
                    "<button type=\"button\" class=\"btn btn-info btn-med\" data-dismiss=\"modal\">&nbsp;&times;&nbsp;Close</button>"
            }
        );
    } else {
        console.log("post_body expected");
    }

}

function show_waitScreen( show ){
    $("#modalOnWait").modal( show?"show":"hide" );
    if ( show ){
        document.getElementById("divModalOnWaitBody").innerHTML = '<p>Please wait...</p><img src="/img/spinner-small.gif" />';
    }
}

function readFile( fileObj, callback ){
    var f = fileObj.files[0];
    if (f) {
        var r = new FileReader();
        r.onload = function(e) {
            var ct = r.result;
            if ( callback )
                callback(ct);
        };
        r.readAsText(f);
    } else {
        alert("Failed to load file");
    }
}

function load_jobQueue( reload ){
    if ( reload === 'undefined' )
        reload = true;

    var p = $.getJSON('/api/jobQueue/engrave', function( data ) {
        var table = document.getElementById("tabPrintQueue");
        table.innerHTML = "";
        if ( data.length > 0 ){
            for (var i in data) {
                var item = data[i];
                var tr = document.createElement('tr');
                tr.className = "table-hover";

                var td = document.createElement('td');
                td.innerHTML = item.order;
                tr.appendChild(td);

                var td = document.createElement('td');
                td.innerHTML = item.description;
                tr.appendChild(td);

                var td = document.createElement('td');
                td.innerHTML = '<button type="button" class="btn btn-info btn-med">' +
                    '<a href="' + item.value + '">' +
                    '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>' +
                    ' Print</a></button>';
                tr.appendChild(td);

                table.appendChild(tr);
            }
        } else {
            var tr = document.createElement('tr');
            tr.className = "table-hover";
            var td = document.createElement('td');
            td.innerHTML = "Nil";
            tr.appendChild(td);
            var td = document.createElement('td');
            tr.appendChild(td);
            var td = document.createElement('td');
            tr.appendChild(td);
            table.appendChild(tr);
        }

        document.getElementById("imgLoadingQueue").style.display = 'none';

    }).always( function(){
        if ( reload )
            window.setTimeout( load_jobQueue, 2000 );
    });

    setTimeout( function(){ p.abort(); }, 2000 );
}