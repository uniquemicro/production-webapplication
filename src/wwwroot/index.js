var argv = Array();

function setArg( id, arg, val ){
    console.log( id, arg, val );
    argv[id][arg] = val;
}

window.onload = function(){

    load_jobQueue();

    $.getJSON('/api/make/', function( data ){
        var table = document.getElementById("tabMakeList");
        for ( var i in data ){
            var item = data[i];
            var tr = document.createElement('tr');
            tr.className = "table-hover";

            var td = document.createElement('td');
            td.innerHTML = item.name;
            tr.appendChild( td );

            var td = document.createElement('td');
            td.innerHTML = item.description;
            tr.appendChild( td );

            var td = document.createElement('td');
            td.innerHTML = '<button id="btnMake'+ item.id + '" type="button" class="btn btn-info btn-med" data-toggle="modal" data-target="#modalOnMake' + item.id + '">' +
                '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>' +
                ' Make</button>';
            tr.appendChild( td );

            table.appendChild( tr );

            var btn = document.getElementById("btnMake" + item.id);
            btn.id = item.id;
            btn.args = item.args;
            if ( item.args ) {
                argv[item.id] = Array(item.args.length);

                btn.onclick = function () {

                    if (document.getElementById("modalOnMake" + this.id)) {
                        console.log("Modal exists, opening");
                        //open only, as already exisits
                    } else {
                        fileCount = 1;

                        //does not exist, create modal and open
                        divModal = document.createElement('div');
                        divModal.id = "modalOnMake" + this.id;
                        divModal.className = "modal fade";
                        divModal.role = "dialog";
                        var html = '<div class="modal-dialog">' +
                            '<!-- Modal content-->' +
                            '<div class="modal-content">' +
                            '<div class="modal-header">' +
                            '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                            '<h4 class="modal-title">Configuration</h4>' +
                            '</div>' +
                            '<div class="modal-body">' +
                            '<p>Enter additional configuration values:</p>' +
                            '<table width="100%">';
                        for (var i = 0; i < this.args.length; i++) {
                            var item = this.args[i];
                            html += '<tr>';
                            html += '<td>' + item.name + ':</td>';
                            html += '<td>';
                            if ( !item.type ){
                                html += '<input type="text" id="inpMake' + this.id + 'Arg' + i + '" onchange="setArg(' + this.id + ',' + i + ',this.value)" /></td>';
                            } else if ( item.type == "COM") {
                                _id = 'inpMake' + this.id + 'Arg' + i;
                                var id = this.id;
                                var arg = i;
                                html += '<select id="inpMake' + this.id + 'Arg' + i + '" style="width: 100%" onchange="setArg(' + this.id + ',' + i + ',this.value)"></select>';
                                $.getJSON('/api/server/serialports', function(data){
                                    console.log(this._id);
                                    sel = document.getElementById( _id );
                                    data.forEach( function(currentValue, index, array){
                                        opt = document.createElement('option');
                                        opt.innerHTML = "[" + currentValue.name + "] ";
                                        if ( currentValue.product ){
                                            opt.innerHTML += currentValue.product;
                                        }
                                        opt.value = currentValue.device;
                                        sel.append( opt );
                                    });
                                    setArg(id, arg, sel.value);
                                });
                            } else if ( item.type == "file" ){
                                html += '<input type="file" ' +
                                    'name="file' + fileCount + '" ' +
                                    'id="inpMake' + this.id + 'Arg' + i + '" ';
                                if ( item.onchange ){
                                    html += 'onchange="' + item.onchange + '"';
                                }
                                html += ' />';
                                if ( item.example ) {
                                    html += 'Example file: <a href="' + item.example + '">Download</a></td>';
                                }
                                fileCount += 1;
                            }
                            html += '</tr>';
                            html += '<tr>';
                            html += '<td colSpan="2" style="font-size: smaller; font-style: italic; padding-bottom: 12px;">' + item.description + '</td>';
                            html += '</tr>'
                        }
                        html += '</table>' +
                            '</div>' +
                            '<div class="modal-footer">' +
                            '<button style="background-color: indianred" type="button" class="btn btn-default" data-dismiss="modal">&times; Close</button>' +
                            '<button id="btnDoMake' + this.id + '" style="background-color: greenyellow" onclick="do_make(' + this.id + ')" type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> Make</button>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                        divModal.innerHTML = html;


                        document.body.appendChild(divModal);

                        var id_focus = 'inpMake' + this.id + 'Arg0';
                        window.setTimeout(function () {
                            try {
                                document.getElementById(id_focus).focus().val("");
                            } catch (err) {};
                        }, 1000);
                        this.setAttribute("data-toggle", "modal");
                        this.setAttribute("modalMake", this.id);
                        this.addEventListener("keypress", function (event) {
                            event.preventDefault();
                            if (event.keyCode === 13) { //on enter key
                                document.getElementById("btnDoMake" + this.id).click();
                            }
                        });
                    }
                }
            } else {
                argv[item.id] = Array(0);
                btn.onclick = function () {
                    do_make( this.id );
                }
            }
        }

        document.getElementById("imgLoading").style.display = 'none';

    });

    load_device_table();


}

function load_device_table(){
    $.get('/log/devices.csv', function( data ){
        var table = document.getElementById("tabDeviceList");
        //clear table body before proceeding
        table.innerHTML = "";
        var lines = data.split("\n");
        for ( var line = lines.length-1; line>=Math.max(lines.length - 20, 0); line-- ){
            var items = lines[ line ].split(",");
            //console.log( items );
            if ( items.length == 1 && items[0]=="" )
                continue;

            var tr = document.createElement('tr');
            tr.className = "table-hover";

            var td = document.createElement('td');
            td.innerHTML = items[0];
            tr.appendChild( td );

            var td = document.createElement('td');
            td.innerHTML = items[1];
            tr.appendChild( td );

            var td = document.createElement('td');
            td.innerHTML = items[2];
            tr.appendChild( td );

            var td = document.createElement('td');
            if ( items.length > 3 )
                td.innerHTML = items[3];
            tr.appendChild( td );

            table.appendChild( tr );

            document.getElementById("imgLoadingDevices").style.display = 'none';

        }
        //continue to refresh table
        //window.setTimeout( load_device_table, 1000 );
    });


}

//this is the function for the sd card make action
function do_make( id, argvLocal ){//argvlocal should be a local method variable and not getting confused with the global one
    if ( id === 'undefined' )
        throw "ID not given";
    var a;
    if ( argvLocal  === 'undefined' || argvLocal === null || typeof(argvLocal) == 'undefined' ){
        a = argv[id].join(" ");
        console.log("Undefined argv");
    } else {
        a = argvLocal;
        console.log("argv should be defined at this point");
    }

    $.post( '/api/make/' + id + (a?((a.length>(argv[id].length-1))?("?argv=" + a):""):"") , function( data ){
        window.location.href = "make/?id=" + id + "&stdout=" + data.stdout;
        return;
    });
}

function parseListFromCSV( fileObj, columnHeaders, callback, args ){
    console.log("parseListFromCSV");
    var f = fileObj.files[0];
    if (f) {
        var r = new FileReader();
        r.columnHeaders = columnHeaders;
        r.obj = fileObj;
        r.args = args;
        r.onload = function(e) {
            var contents = e.target.result;
            var ct = r.result;
            var rows = ct.split('\n');
            var columnNumbers = new Array();
            var submitValues = {};
            r.columnHeaders.forEach( function( value, index, array ) {
                submitValues[value] = [];
            });
            rows.forEach( function( value, index, array ) {
                value = value.replace("\r", "");
                if (index == 0) {
                    var headers = value.split(",");
                    headers.forEach(function (headerValue, headerIndex, headerArray) {
                        r.columnHeaders.forEach( function( key, keyIndex, keyArray ) {
                            // console.log(key, keyIndex, headerValue, headerValue == key);
                            if ( headerValue === key ){
                                columnNumbers.push(headerIndex);
                            };
                        });
                    });
                } else {
                    if ( value !== "" ){
                        var values = value.split(",");
                        columnNumbers.forEach(function (number, numberIndex, numberArray) {
                            var key = r.columnHeaders[numberIndex];
                            var val = values[number];
                            submitValues[key].push(val);
                        });
                    }
                }
            });
            console.log( submitValues );

            var i = 0;
            var str = "";
            for (var first in submitValues) break;
            for ( var row = 0; row< submitValues[first].length; row++ ){
                r.columnHeaders.forEach( function( key, keyIndex, keyArray ) {
                    str += submitValues[key][row];
                    if ( keyIndex < keyArray.length - 1) {
                        str += ",";
                    }
                });
                str += " ";
            }


            if ( callback ){
                callback( args[0], args[1], str);
            }
        };
        r.readAsText(f);
    } else {
        alert("Failed to load file");
    }
}

function load_jobQueue(){
    $.getJSON('/api/jobQueue/make', function( data ) {
        var table = document.getElementById("tabMakeQueue");
        table.innerHTML = "";
        if ( data.length > 0 ){
            for (var i in data) {
                var item = data[i];
                var tr = document.createElement('tr');
                tr.className = "table-hover";

                var td = document.createElement('td');
                td.innerHTML = item.order;
                tr.appendChild(td);

                var td = document.createElement('td');
                td.innerHTML = item.description;
                tr.appendChild(td);

                var td = document.createElement('td');
                var btn = document.createElement('button');
                btn.type    =   "button";
                btn.className = "btn btn-info btn-med";
                btn.id = item.id;
                btn.part = item.part;
                btn.argv = item.value;
                btn.onclick = function(){
                    do_make( this.part, this.argv );
                };
                btn.innerHTML = '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> Make';
                td.appendChild(btn);

                tr.appendChild(td);

                table.appendChild(tr);
            }
        } else {
            var tr = document.createElement('tr');
            tr.className = "table-hover";
            var td = document.createElement('td');
            td.innerHTML = "Nil";
            tr.appendChild(td);
            var td = document.createElement('td');
            tr.appendChild(td);
            var td = document.createElement('td');
            tr.appendChild(td);
            table.appendChild(tr);
        }

        document.getElementById("imgLoadingQueue").style.display = 'none';
        window.setTimeout( load_jobQueue, 5000 );
    });
}

