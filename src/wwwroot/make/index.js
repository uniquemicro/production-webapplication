var url = new URL(window.location.href);
var socket;

$(document).on("DOMSubtreeModified propertychange", ".box", function(e) {
    $(this).animate({ scrollTop: $(this).prop("scrollHeight") }, 100);
});

function load_console(){
    window.setTimeout(function () {
        try {
            document.getElementById("inpConsoleUserInput").focus().val("");
        } catch (err) {};
    }, 1000);

    document.getElementById("inpConsoleUserInput").addEventListener("keypress", function (event) {
        if (event.keyCode === 13) { //on enter key
            event.preventDefault();
            document.getElementById("btnConsoleUserInput").click();
        }
    });

    var port = url.searchParams.get("stdout");
    console.log( "Opening new web socket" );
    console.log( "ws://" + window.location.hostname + ":" + port );
    socket = new WebSocket("ws://" + window.location.hostname + ":" + port);
    socket.onmessage = function(event) {
        var console = document.getElementById("divConsole");
        var msg = event.data.toString();
        console.innerHTML = console.innerHTML + msg.replace(/\n/g, "<br>");
        window.scrollTo(0, getDocHeight());
    };
    socket.onclose = function(event) {
        var footer = document.getElementById("divFooter");
        footer.innerHTML = "Task complete!";
        finish_footer();
    }
    socket.onerror = function(event){
        var footer = document.getElementById("divFooter");
        footer.innerHTML = "The process may not have started.";
        socket.onclose = function(event){};
        finish_footer();
    }

}

function send() {
    var obj = document.getElementById("inpConsoleUserInput");
    var text = obj.value;
    text += "\n";

    socket.send( text );
    obj.innerHTML = "";

    var console = document.getElementById("divConsole");
    console.innerHTML += "> " + text;
    obj.value = "";

}

function finish_footer(){
    var footer = document.getElementById("divFooter");
    footer.appendChild( document.createElement("hr") );

    var a = document.createElement('a');
    a.href = "/";
    a.className="btn btn-default btn-large";
    a.innerHTML = '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Home';
    footer.appendChild( a );
}

function getDocHeight() {
    var D = document;
    return Math.max(
        Math.max(document.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(document.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(document.body.clientHeight, D.documentElement.clientHeight)
    );
}
