﻿// This script converts any <time> elements on the page into the browsers local time format

// used for testing the script has loaded
function TimeConversion() {
    return true;
}

function convertDateTimes() {
    console.log("Translating datetimes");
    var items = document.getElementsByTagName("time");
    for (var i = 0; i < items.length; i++) {
        var enableconversion = items[i].getAttribute("auto-convert");

        if (!enableconversion || !JSON.parse(enableconversion)) {
            continue;
        }

        var servertimestring = items[i].getAttribute("datetime");
        var timestyle = items[i].getAttribute("output-style");
        var appendtime = items[i].getAttribute("append");

        var parseddate = new Date(servertimestring);
        console.debug("Original input time: " + servertimestring);
        console.debug("js Date toString: " + parseddate.toString());
        var outputtimetext;

        if (!timestyle || timestyle == "short") {
            outputtimetext = toLocalIsoString(parseddate, true, false);
        }
        else if (timestyle == "shortwithtimezone") {
            outputtimetext = toLocalIsoString(parseddate, true, true);
        }
        else if (timestyle == "iso") {
            outputtimetext = toLocalIsoString(parseddate, false, true);
        }
        else if (timestyle == "browser") {
            outputtimetext = parseddate.toString();
        }
        else {
            console.log(timestyle + " is a bad time style");
            outputtimetext = parseddate.toString();
        }

        if (appendtime && JSON.parse(appendtime)) {
            items[i].innerText += outputtimetext;
        }
        else {
            items[i].innerText = outputtimetext;
        }
    }
}

function toLocalIsoString(date, spaceinsteadoft, includetimezone) {
    function pad(n) { return n < 10 ? '0' + n : n }
    var localIsoString = date.getFullYear() + '-'
        + pad(date.getMonth() + 1) + '-'
        + pad(date.getDate()) + (spaceinsteadoft ? ' ' : 'T')
        + pad(date.getHours()) + ':'
        + pad(date.getMinutes()) + ':'
        + pad(date.getSeconds())
        + (includetimezone ? getOffsetFromUTC() : "");
    return localIsoString;
};

function getOffsetFromUTC() {
    var offset = new Date().getTimezoneOffset();

    if (offset == 0) {
        return "Z";
    }

    return ((offset < 0 ? '+' : '-')
        + pad(Math.abs(offset / 60), 2)
        + pad(Math.abs(offset % 60), 2))
};

// Pad a number to length using padChar
function pad(number, length, padChar) {
    if (typeof length === 'undefined') length = 2;
    if (typeof padChar === 'undefined') padChar = '0';
    var str = "" + number;
    while (str.length < length) {
        str = padChar + str;
    }
    return str;
}

$(document).ready(function () {
    convertDateTimes();
});