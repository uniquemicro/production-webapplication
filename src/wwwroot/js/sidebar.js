﻿// Write your Javascript code.
function manageSidebar(selected) {
    if (selected == '') {
        var status = document.getElementById("sideBar").classList.contains("sideBar-minimised");
        collapseSidebar();
        if (status) {
            expandSidebar("sideBar");
        }
    }
    else {
        collapseSidebar();
        expandSidebar(selected);
    }
}

function collapseSidebar() {
    //get a list of all the labels and collapse them
    var c = document.getElementsByClassName("sideBar-Label");
    for (i = 0; i < c.length; i++) {
        document.getElementById(c[i].id).classList.add("label-minimised");
        document.getElementById(c[i].id).classList.remove("label-expanded");
    }

    //get a list of all the filter panels and collapse them
    var f = document.getElementById("filters").children;
    for (i = 0; i < f.length; i++) {
        document.getElementById(f[i].id).classList.add("filter-minimised");
        document.getElementById(f[i].id).classList.remove("filter-expanded");
    }
    document.getElementById("expandBtn").classList.add("glyphicon-menu-right")
    //document.getElementById("expandBtn").classList.add("pull-left")
    document.getElementById("expandBtn").classList.remove("glyphicon-menu-left")
    //document.getElementById("expandBtn").classList.remove("pull-right")

    document.getElementById("main").classList.add("content-expanded");
    document.getElementById("main").classList.remove("content-shrunk");
    document.getElementsByTagName("footer")[0].classList.add("content-expanded");
    document.getElementsByTagName("footer")[0].classList.remove("content-shrunk");
    document.getElementById("sideBar").classList.add("sideBar-minimised");
    document.getElementById("sideBar").classList.remove("sideBar-expanded");
}

function expandSidebar(selected) {
    if (selected == "sideBar") {
        document.getElementById("sideBar").classList.remove("sideBar-minimised");
        document.getElementById("sideBar").classList.add("sideBar-expanded");

        //get a list of all the labels and collapse them
        var c = document.getElementsByClassName("sideBar-Label");
        for (i = 0; i < c.length; i++) {
            document.getElementById(c[i].id).classList.remove("label-minimised");
            document.getElementById(c[i].id).classList.add("label-expanded");
        }
        document.getElementById("expandBtn").classList.remove("glyphicon-menu-right")
        document.getElementById("expandBtn").classList.add("glyphicon-menu-left")
    }
    else {
        document.getElementById(selected).classList.remove("filter-minimised");
        document.getElementById(selected).classList.add("filter-expanded");
    }
    document.getElementById("main").classList.remove("content-expanded");
    document.getElementById("main").classList.add("content-shrunk");
    document.getElementsByTagName("footer")[0].classList.remove("content-expanded");
    document.getElementsByTagName("footer")[0].classList.add("content-shrunk");
}