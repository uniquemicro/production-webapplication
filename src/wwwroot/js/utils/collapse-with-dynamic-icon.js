/**
 * Sets up collapse behavour with glyphicon switching for any element with the
 * 'data-toggle="collapse-with-dynamic-icon"' attribute set
 *
 * Expects the triggering element to also have 'open' and 'closed' classes
 * defined as data-attributes
 */
$(function () {
    // Constants
    //----------

    var TRIGGER_SELECTOR = '[data-toggle="collapse-with-dynamic-icon"]';
    var CLOSED_CLASS_DATA_ITENTIFIER = 'collapse-closed-class';
    var OPEN_CLASS_DATA_ITENTIFIER = 'collapse-open-class';

    // Helper functions
    //-----------------

    // Copied from Bootstrap collapse plugin
    function getTargetFromTrigger($trigger) {
        var href
        var target = $trigger.attr('data-target')
            || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') // strip for ie7

        return $(target)
    }

    // Main
    //-----

    // Find all trigger elements on the page
    var triggers = $(TRIGGER_SELECTOR);

    // Output warnings for any incorrectly configured trigger element
    triggers.each(function (i, e) {
        var $trigger = $(e);

        if (!$trigger.data(CLOSED_CLASS_DATA_ITENTIFIER)) {
            console.warn('collapse-with-dynamic-icon has been used without a close class for the following element. ' +
                         'Icon switching will not work unless the \'data-' + CLOSED_CLASS_DATA_ITENTIFIER + '\' attribute is set');
            console.warn(e)
        }

        if (!($trigger.data(OPEN_CLASS_DATA_ITENTIFIER))) {
            console.warn('collapse-with-dynamic-icon has been used without open class for the following element. ' +
                         'Icon switching will not work unless the \'data-' + OPEN_CLASS_DATA_ITENTIFIER + '\' attribute is set')
            console.warn(e)
        }
    })

    
    triggers.on('click.collapse-with-dynamic-icon', function (e) {
        var $trigger = $(this)

        // prevent default click event if href has been used instead of data-target
        if (!$trigger.attr('data-target')) e.preventDefault()

        var $target = getTargetFromTrigger($trigger)

        // return early if a transition is already in progress
        if ($target.hasClass('collapsing')) return;

        var $triggerToggleIcon = $trigger.find('span.glyphicon');

        var openIconClass = $trigger.data(OPEN_CLASS_DATA_ITENTIFIER);
        var closedIconClass = $trigger.data(CLOSED_CLASS_DATA_ITENTIFIER);

        // Don't do an icon switch unless both icons were defined
        var doIconChange = (openIconClass && closedIconClass)

        if ($target.hasClass('collapse in')) {
            // filters are shown
            $target.collapse('hide');
            if (doIconChange) {
                $triggerToggleIcon.removeClass(openIconClass);
                $triggerToggleIcon.addClass(closedIconClass);
            }
        } else {
            // filters are already collapsed
            $target.collapse('show');
            if (doIconChange) {
                $triggerToggleIcon.removeClass(closedIconClass);
                $triggerToggleIcon.addClass(openIconClass);
            }
        }
    })
});
