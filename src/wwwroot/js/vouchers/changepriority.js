﻿/**
 * Shared AJAX functionality for 'change priority' buttons on User/Device pages
 * in OfferRedemptionAccess area of Vouchers service
 */

// Wrap all code in DOMLoaded event handler
$(function () {
    /**
     * Helper function for error messages
     */
    var buildErrorMsg = function (jqXHR) {
        var alertMsg = 'Something went wrong changing the priority for this offer';
        alertMsg += ' (HTTP status ' + jqXHR.status.toString() + ').' + "\n";
        if (jqXHR.responseText.length > 0) {
            alertMsg += 'Server response: ' + jqXHR.responseText + "\n";
        }
        alertMsg += 'Please try again later.';
        return alertMsg
    }

    /**
     * Shared function for performing a priority up/down change
     *
     * @param $btn - jQuery-selected button to read URL from and show AJAX spinner on
     * @param priorityChangeIconClass - class name of glyphicon used to represent the type of priority change (up/down)
     */
    var doPriorityChangeAjax = function ($btn, priorityChangeIconClass) {
        var postUrl = $btn.data('priority-url')

        $btn.addClass('disabled')
            .find('.glyphicon')
            .removeClass(priorityChangeIconClass)
            .addClass('glyphicon-animate-spin glyphicon-repeat');

        $.post(postUrl, function (data, textStatus, jqXHR) {
            // reload page to show new priority
            window.location.reload();
        })
            .fail(function (jqXHR, textStatus, errorThrown) {
                window.alert(buildErrorMsg(jqXHR));

                $btn.removeClass('disabled')
                    .find('.glyphicon')
                    .removeClass('glyphicon-animate-spin glyphicon-repeat')
                    .addClass(priorityChangeIconClass);
            })
    }

    $('.btn-decrease-priority').on('click', function (e) {
        // use 'this' to get the btn - e.target might be inner span
        var $btn = $(this);

        doPriorityChangeAjax($btn, 'glyphicon-chevron-down')
    })

    $('.btn-increase-priority').on('click', function (e) {
        // use 'this' to get the btn - e.target might be inner span
        var $btn = $(this);

        doPriorityChangeAjax($btn, 'glyphicon-chevron-up')

    })
})