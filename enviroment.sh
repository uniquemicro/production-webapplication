export http_proxy=http://192.168.13.1:8000/
export https_proxy=http://192.168.13.1:8000/

add-apt-repository ppa:deadsnakes/ppa
apt-get update -y
apt-get upgrade -y
apt-get install -y libssl-dev

# onscreen keyboard, remote desktop
apt-get install -y matchbox-keyboard xrdp

# applications required to run application
apt-get install -y python3-dev python3-setuptools python3-pip

wget https://www.openssl.org/source/openssl-1.1.0g.tar.gz
gzip -d openssl-1.1.0g.tar.gz
tar -x -f openssl-1.1.0g.tar
cd openssl-1.1.0g
./configure --prefix=/usr
make
sudo make install
cd ..
sudo rm -r openssl-1.1.0g
rm openssl-1.1.0g.tar

wget https://www.python.org/ftp/python/3.4.2/Python-3.4.2.tgz
gzip -d Python-3.4.2.tgz
tar -x -f Python-3.4.2.tar
cd Python-3.4.2
./configure --prefix=/usr
make
sudo make install
cd ..
sudo rm -r Python-3.4.2
rm Python-3.4.2.tar

pip3 -V

apt-get install -y apache2
apt-get install -y mysql-server mysql-client
apt-get install -y phpmyadmin php-mbstring php-gettext

# prerequisites for `pip3 install Pillow`, for PDF generation
apt-get install libtiff5-dev libjpeg-dev zlib1g-dev \
    libfreetype6-dev liblcms2-dev libwebp-dev libharfbuzz-dev libfribidi-dev \
    tcl8.6-dev tk8.6-dev python-tk

cat << 'EOF' > /etc/apache2/sites-available/phpmyadmin.conf
Listen 8080
<VirtualHost *:8080>
        Include /etc/phpmyadmin/apache.conf
</VirtualHost>
EOF
cat << 'EOF' > /etc/iptables/rules.ipv4
-A INPUT -p tcp --dport 80 -j ACCEPT
-A INPUT -p tcp --dport 8080 -j ACCEPT
EOF

phpenmod mcrypt
phpenmod mbstring
a2enmod proxy
a2enmod proxy_http
a2enmod proxy_balancer
a2enmod lbmethod_byrequests
a2ensite phpmyadmin.conf


# download application
# must create new key per device
ssh-keygen
cat "Copy key to 'https://bitbucket.org/uniquemicro/production-webapplication/admin/access-keys/'"
wait
ssh -T git@bitbucket.org
git clone git@bitbucket.org:uniquemicro/production-webapplication.git

# python packages
pip3 install -r ~/production-webapplication/requirements.txt
mysql --user=root --password=root --force < ~/production-webapplication/mySQL_setup.sql


unset http_proxy
unset https_proxy

reboot

