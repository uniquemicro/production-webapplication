#!/usr/bin/python3

import serial, time, sys, traceback
import UMD.Database

DEBUG = False
ERROR_CODE = 0

tag = sys.argv[1]
PORT = sys.argv[2]
order = sys.argv[3]
#tag = "340F7A427200100011309326"
#tag = "340F7A427200100011309324"

#
# Open serial port
#
s = None

try:
    s = serial.Serial(PORT, write_timeout=2, timeout=2)
except:
    print ( "Failed to open serial port" )
    exit(22)

try:
    s.reset_input_buffer()
    s.reset_output_buffer()
    cmd = str()
    s.write((cmd + "\n").encode())
    s.flush()
    if DEBUG:
        print(PORT, "<- ", cmd)
    time.sleep(0.1)
    intro = bytes()
    while ((intro[-1] != 0x3a) if len(intro) > 0 else True):
        intro += s.read()

    if DEBUG:
        print(PORT, "-> ", intro.decode())

    #
    # Write tag with value
    #

    cmd = "W " + tag
    s.write((cmd + "\n").encode('utf-8'))
    s.flush()
    if DEBUG:
        print(PORT, "<- ", cmd)
    time.sleep(0.1)
    result1 = bytes()
    while ((result1[-1] != 0x3a) if len(result1) > 0 else True):
        result1 += s.read()
    if DEBUG:
        print(PORT, "-> ", result1.decode().replace(cmd, ""))

    #
    # Read tag to check value
    #

    cmd = "R 5"
    s.write((cmd + "\n").encode())
    s.flush()
    if DEBUG:
        print(PORT, "<- ", cmd)
    time.sleep(0.1)
    result2 = bytes()
    while ((result2[-1] != 0x3a) if len(result2) > 0 else True):
        result2 += s.read()

    result = result2.decode('utf-8').replace(cmd, "").replace("\\n", "").replace("\\r", "").replace(":", "").replace("\n",
                                                                                                                     "").replace(
        "\r", "")
    if DEBUG:
        print(PORT, "-> ", str(result))

    if (tag == result):
        print("Success!")
    else:
        print("Fail!", tag, "!=", result)
        ERROR_CODE = 255
except Exception as e:
    print("Failed to encode, here is the stacktrace")
    print(traceback.format_exc())
    s.close()
    ERROR_CODE = 255
finally:
    s.close()


#
# Notify production log
#
try:
    cnx = UMD.Database.open()
    hndl = cnx.cursor()
    query = "INSERT INTO `productionLog` " \
            "(`partNumber`, `serial`, `productionJob`, `customer`, `parameters`, `comments`) " \
            "VALUES (" \
            "'7-4000-016-7', " \
            "'" + tag + "', " \
            "'" + order + "', " \
            " 0, " \
            "'nil', " \
            "'error_code=" + str(ERROR_CODE) + "," + (("failed, " + tag + "!=" + result) if tag != result else "nil") + "'" \
            ")"
    if DEBUG:
        print(query)
    hndl.execute(query)
    UMD.Database.commit()
except Exception as e:
    print( "Failed to record encoding into Production database" )
finally:
    hndl.close()
    cnx.close()
    # print("Success")

exit(ERROR_CODE)