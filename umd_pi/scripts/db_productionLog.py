#!/usr/bin/python3

import UMD.Database
import copy, time, sys

part = sys.argv[1]
serial = sys.argv[2]
productionJob = sys.argv[3]
customer = int(sys.argv[4])
parameters = sys.argv[5]
comments = (sys.argv[6]).replace("'", "''")
cnx = UMD.Database.open()
hndl = cnx.cursor()

query = """
INSERT INTO `productionLog` (`partNumber`, `serial`, `productionJob`, `customer`, `parameters`, `comments`) 
VALUES (
    '{0:s}',
    '{1:s}',
    '{2:s}',
    {3:d},
    '{4:s}',
    '{5:s}'
    );
""".format( part, serial, str(productionJob).upper(), customer, parameters, comments)

hndl.execute( query )
UMD.Database.commit()
hndl.close()
cnx.close()