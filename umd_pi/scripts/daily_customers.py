#!/usr/bin/python3

import UMD.Database
import Cin7
import copy, time, datetime


modifiedSince = datetime.datetime.utcnow() - datetime.timedelta(days=21)

rows    = 50
p       = 1
while rows==50:

    print("Request page ", p)
    customers = Cin7.Contacts(
        order="id",
        page=p,
        where="modifiedDate > '" + modifiedSince.strftime("%Y-%M-%D %h:%m:%s") + "'"
    )
    cnx = UMD.Database.open()
    hndl = cnx.cursor()

    print("Returning, parsing query")
    p += 1
    rows = len(customers)

    if rows == 0:
        continue

    keys = copy.deepcopy(list(customers[0].keys()))

    query = "INSERT INTO `data`.`customers` (`" + "`, `".join(keys) + "`) " + \
            "VALUES "
    query = query.replace("group", "_group")

    custValues = list()

    try:

        for c in customers:

            vals = list()
            for j in range(len(keys)):
                try:
                    key = keys[j]
                    val = str(c[key])
                    val = val.replace("'", "''")
                    val = val.replace("True", "1").replace("False", "0")
                    if val.endswith("\\"):
                        val += " "
                    ## INT VALUES
                    if key in ("id", "salesPerson", "accountNumber", "billingId"):
                        val_int = 0
                        try:
                            if val == "None":
                                val_int = 0
                            elif val != "":
                                val_int = int(val)
                        except ValueError as e:
                            val_int = 0
                        finally:
                            val = str(val_int)
                    elif key in ("modifiedDate", "createdDate"):
                        val = "'" + val[:19].replace("T", " ") + "'"
                    elif key in ("secondaryContacts"):
                        val = "' '"
                    else:
                        val = "'"+val.replace("(", "").replace(")", "").replace("\n", "\\n").replace("\r", "").replace(",", " ") +"'"

                    if ( val[-1] == "'" and val[0] != "'" ):
                        val="'"+val
                finally:
                    vals.append( val.encode('UTF-8', errors="BACKSLASHREPLACE").decode() )

            if len(vals)==len(keys):
                custValues.append( "(" + str(", ".join(vals)) + ")" )
            else:
                print( "Bad customer, customer==" + repr(c) )
                continue

    finally:
        pass
    query += ", ".join(custValues)
    query += " ON DUPLICATE KEY UPDATE "

    updater = list()
    for key in keys:
        updater.append(("{0:s} = VALUES({0:s})".format(key)).replace("group", "_group"))

    query += ', '.join(updater)
    query += ";"
    print( query )
    print( "Executing...")

    hndl.execute( query, "" )
    UMD.Database.commit()
    hndl.close()
    cnx.close()


print("Done")