#!/bin/bash

echo "Start"
THIS_DIR=/home/umd/bin/rfid-encode

cd $THIS_DIR
echo "argv==$@"

ORDER_NUMBER=$1
COM_PORT=$2

echo ""
echo ""
echo "Order Number $ORDER_NUMBER"

for var in "$@"
do
    if [ "$var" = "$1" ]
    then
        continue
    fi

    if [ "$var" = "$2" ]
    then
        continue
    fi

    IFS=','
    read -r -a THIS_TAG <<< "${var}"
    VISUAL=${THIS_TAG[0]}
    EPC=${THIS_TAG[1]}

    echo "Confirm tag visual '$VISUAL', [Y]es to encode, [N]o to skip, [Q]uit:"
    read resp
    resp=`echo $resp | tr -d '\n' | tr -d '\r'`

    success=0
    while [ "$success" = "0" ]
    do

        if [ "$resp" = "Q" ]
        then
            echo ""
            exit 0
        elif [ "$resp" = "N" ]
        then
            success=1
            continue
        else

            echo "Encoding tag '$EPC'..."
            python3 /home/umd/scripts/m652_encode.py $EPC $COM_PORT $ORDER_NUMBER
            if [ "$?" = "0" ]
            then
                echo "Done."
                success=1
            else
                echo ""
                echo "TAG FAILED TO ENCODE!"
                echo "Repeat visual '$VISUAL'? [Y]es to encode, [N]o to skip, [Q]uit:"
                read resp
                resp=`echo $resp | tr -d '\n' | tr -d '\r'`
            fi

        fi

    done
done
