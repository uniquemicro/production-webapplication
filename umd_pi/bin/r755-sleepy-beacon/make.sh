#!/bin/sh
# set -o xtrace
echo "Start"

LOG=
THIS_DIR=/home/umd/bin/r755-sleepy-beacon
APP_LOCATION=$THIS_DIR
PROGRAMMER_IP=192.168.1.7

cd $THIS_DIR
echo "Ready to program? [Y]es to program, [Q]uit:"
read resp
resp=`echo $resp | tr -d '\n' | tr -d '\r'`

success=0
while [ "$success" = "0" ]
do

    if [ "$resp" = "Q" ]
    then

        echo ""
        exit 0

    else

        echo "Getting latest firmware..."
        BINARY=$(ls ${APP_LOCATION}/r755-sleepy-beacon*.hex)
        LOG=${LOG}$"BINARY="${BINARY}"\n"
        LOG=${LOG}$"PROGRAMMER_IP="${PROGRAMMER_IP}$"\n"
        cat ${BINARY} | ssh -i /home/umd/.ssh/umd_pi_root_ssh_key root@${PROGRAMMER_IP} 'cat > /tmp/firmware.hex'

        echo "Run flash.sh"
        LOG=${LOG}$"\nflash.sh OUTPUT:\n**********************\n"
        LOG=${LOG}$(ssh  -i /home/umd/.ssh/umd_pi_root_ssh_key root@${PROGRAMMER_IP} 'bash -s' < ${APP_LOCATION}/flash.sh)$"\n"
        LOG=${LOG}$"**********************\n"

        /home/umd/scripts/db_productionLog.py 3-3040-002-6 $1 0 0 nil "error_code="$?$"\n"${LOG}

        if [ "$?" = "0" ]
        then
            success=1
            echo "Success"
        else
            echo ""
            echo "FAILED TO PROGRAM"
            echo "Repeat [Y]es to encode, [Q]uit:"
            read resp
            resp=`echo $resp | tr -d '\n' | tr -d '\r'`
        fi

    fi

done


