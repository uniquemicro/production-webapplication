# Board power: GPIO9, set as output LOW to turn power board, output HIGH to turn power off
# DEBUG: GPIO24
# BLE_PWR : GPIO23

# set -o xtrace

trap 'echo "Cleaning up"; rm /tmp/avrdude.txt; echo "Unexporting GPIOs... "; echo 9 > /sys/class/gpio/unexport; echo 23 > /sys/class/gpio/unexport; echo 24 > /sys/class/gpio/unexport; echo 17 > /sys/class/gpio/unexport; echo 27 > /sys/class/gpio/unexport; echo "Unexporting GPIOs... Done"; echo "Production log OUTPUT:"; echo "*****"; echo ${PROD_LOG}; echo "*****";' EXIT

PROD_LOG=
# Set board GND pin as output
sudo echo 9 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio9/direction

# Turn board off
echo "Power cycle board"
echo 1 > /sys/class/gpio/gpio9/value
sleep 0.1
# Turn board on
echo 0 > /sys/class/gpio/gpio9/value
echo "Power cycle board... Done"

echo "Flashing board"
BOARD_TYPE=attiny13
echo "BOARD_TYPE="${BOARD_TYPE}
FUSE_BYTE_HIGH=0xFD
echo "FUSE_BYTE_HIGH="${FUSE_BYTE_HIGH}
FUSE_BYTE_LOW=0x6A
echo "FUSE_BYTE_LOW="${FUSE_BYTE_LOW}

# echo "CMD=avrdude -p ${BOARD_TYPE} -C avrdude_gpio.conf -c pi_1 -q -u -Uhfuse:w:${FUSE_BYTE_HIGH}:m -Ulfuse:w:${FUSE_BYTE_LOW}:m -U flash:w:firmware.hex:i -l /tmp/avrdude.txt"
avrdude -p ${BOARD_TYPE} -C avrdude_gpio.conf -c pi_1 -q -u -Uhfuse:w:${FUSE_BYTE_HIGH}:m -Ulfuse:w:${FUSE_BYTE_LOW}:m -U flash:w:firmware.hex:i -l /tmp/avrdude.txt
PROD_LOG=${PROD_LOG}$"\navrdude output: \n***\n"`cat /tmp/avrdude.txt`$"\n***\n"
if [ $? -ne 0 ]
then
	echo "Error in flashing firmware to board"
	echo ""
	cat /tmp/avrdude.txt
	echo ""
	echo "returning"
	exit 5
	# return I/O error 
fi
echo "Flashing board... Done"

echo "Initalising test routine"
echo 23 > /sys/class/gpio/export
echo in > /sys/class/gpio/gpio23/direction
echo 24 > /sys/class/gpio/export
echo in > /sys/class/gpio/gpio24/direction

# Restart board for test
echo 1 > /sys/class/gpio/gpio9/value
sleep 0.1
echo 0 > /sys/class/gpio/gpio9/value
sleep 0.1

echo 17 > /sys/class/gpio/export
echo in > /sys/class/gpio/gpio17/direction
echo 27 > /sys/class/gpio/export
echo in > /sys/class/gpio/gpio27/direction

echo "Starting test."
sleep 0.2
status=`cat /sys/class/gpio/gpio23/value | tr -d '\n' | tr -d '\r'`
echo "BLE power is ${status}"
if [ ${status} != "0" ]
then
	echo "Self test returned ${status}"
	echo "PROGRAMMING FAILED"
	exit 22
	# return Invalid argument
fi

echo "Test complete"
echo "Device ready"

exit 0
