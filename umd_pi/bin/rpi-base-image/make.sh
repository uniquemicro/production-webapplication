#!/bin/sh
THIS_DIR=/home/umd/bin/rpi-base-image
TARGET_PATH=/dev/sde

cd $THIS_DIR
echo " ~~Writing `ls umd_pi_image_v*` to $TARGET_PATH~~ "
sudo pv `ls umd_pi_image_v*` -s 2195M -i 5 -w 75 --force | sudo dd bs=4M conv=fsync of=$TARGET_PATH
echo "Finish"