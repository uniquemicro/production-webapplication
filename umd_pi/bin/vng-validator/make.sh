#!/bin/sh
echo "Start"
THIS_DIR=/home/umd/bin/vng-validator
IMG_DIR=/home/umd/bin/rpi-base-image
TARGET_PATH=/dev/sde
APP_LOCATION=$THIS_DIR
APPS_FOLDER_LOCATION=/home/umd/apps
APP_TARGET1_NAME=vng-controller
APP_TARGET1=$APPS_FOLDER_LOCATION/$APP_TARGET1_NAME
APP_TARGET2_NAME=vng-controller-out
APP_TARGET2=$APPS_FOLDER_LOCATION/$APP_TARGET2_NAME
MOUNT_POINT=/sd_card
REQUIRE_DEVICE_ID=true


cd $THIS_DIR
#set -o xtrace
echo "argv==$@"

echo "Flashing SD Card..."
echo "Writing `ls $IMG_DIR/umd_pi_image_v*` to $TARGET_PATH"
sudo pv `ls $IMG_DIR/umd_pi_image_v*` -s 2195M -i 5 -w 75 --force | sudo dd bs=4M conv=fsync of=$TARGET_PATH
echo "Flashing SD Card... Done"

echo "Mounting device..."
mkdir $MOUNT_POINT
mount ${TARGET_PATH}2 $MOUNT_POINT
mount ${TARGET_PATH}1 $MOUNT_POINT/boot
echo "Mounting device... Done"

trap 'echo "Unmounting device... "; umount $MOUNT_POINT/boot; umount $MOUNT_POINT; rmdir $MOUNT_POINT; echo "Done"' EXIT

#############################

echo "Add device id ..."
if [ -z "$1" ] 
then

if [ "$REQUIRE_DEVICE_ID" -eq "true" ]
then
echo "No device id supplied, exit 1"
exit 1
fi

else
cat << EOF > $MOUNT_POINT/boot/umd_device_id.txt
$1
EOF
fi
echo "Add device id ... Done"

echo "Write model number..."
cat << 'EOF' > $MOUNT_POINT/boot/umd_model.txt
M800AR100V1
EOF
echo "Done"

#############################

echo "Unzip compiled app... "
cd $MOUNT_POINT/root
tar xvf $APP_LOCATION/demo.tar 
cd -
echo "Done"

#############################

echo "Unzip demo/test script... "
mkdir $MOUNT_POINT$APPS_FOLDER_LOCATION
echo "- Controller"
unzip -o -q `ls $APP_LOCATION/vng-controller*` -d $MOUNT_POINT$APPS_FOLDER_LOCATION
echo "- Validation Server"
unzip -o -q `ls $APP_LOCATION/vast-local*` -d $MOUNT_POINT$APPS_FOLDER_LOCATION
echo "Done"

#############################

echo "Setup watchdog service..."
mkdir $MOUNT_POINT/etc/watchdog.d

cat << 'EOF' > $MOUNT_POINT/etc/watchdog.d/reset
#!/bin/sh -e
# try setup button if not already done
{
	echo 22 >  /sys/class/gpio/export
	echo in > /sys/class/gpio/gpio22/direction
} || {
	echo "Reset button already setup, continuing"
}
#do check of push button
if [ $(cat /sys/class/gpio/gpio22/value) = "0" ]
then 
	echo "Push button pressed, returning SRESET"
	exit 254;
else 
	exit 0;
fi

EOF

chmod +x $MOUNT_POINT/etc/watchdog.d/reset

cat << 'EOF' >  $MOUNT_POINT/etc/watchdog.conf
#do watchdog checks
watchdog-device = /dev/watchdog0
watchdog-timeout = 14
#after x seconds if dog has not been patted, restart
realtime = yes
priority = 1
interval = 10
#do check every interval if successful pat dog.
max-load-1 = 24
#check max load
EOF

#############################

echo "Setup watchdog permissions service..."

cat << 'EOF' > $MOUNT_POINT/etc/systemd/system/umd-watchdogpermissions.service
[Unit]
Description=Set Watchdog permissions

[Service]
ExecStart=/root/bin/umd-watchdogpermissions

[Install]
WantedBy=multi-user.target vng-controller.service
EOF

cat << 'EOF' > $MOUNT_POINT/root/bin/umd-watchdogpermissions
#!/bin/bash
chown root:watchdog /dev/watchdog
chmod g+rw /dev/watchdog
exit 0
EOF

chmod +x $MOUNT_POINT/root/bin/umd-watchdogpermissions
echo "Done"

#############################

echo "Setup hostname service..."
cat << 'EOF' > $MOUNT_POINT/etc/systemd/system/umd-sethost.service
[Unit]
Description=Set UMD Device Hostname

[Service]
ExecStart=/root/bin/umd-sethostname

[Install]
WantedBy=multi-user.target
EOF
cat << 'EOF' > $MOUNT_POINT/root/bin/umd-sethostname
#!/bin/bash

hn=`cat /boot/umd_model.txt|tr '[:upper:]' '[:lower:]'`_`cat /sys/class/net/eth0/address|tr -d ':' |tr '[:upper:]' '[:lower:]'`
hostnamectl set-hostname ${hn}

exit
EOF
chmod +x $MOUNT_POINT/root/bin/umd-sethostname
echo "Done"

#############################

echo "Setup bonjour/mDNS/avahi service..."
cat << 'EOF' > $MOUNT_POINT/etc/avahi/services/umd.service
<?xml version="1.0" standalone='no'?>
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
	<name replace-wildcards="yes">%h</name>
	<service>
		<type>_device-info._tcp</type>
		<port>0</port>
		<txt-record>model=m800ar100v1</txt-record>
	</service>
	<service>
		<type>_ssh._tcp</type>
		<port>22</port>
	</service>
	<service>
		<type>_http._tcp</type>
		<port>80</port>
		<txt-record>service=VAST-Validator</txt-record>
	</service>
</service-group>
EOF

echo "Done"

#############################

echo "Setup beep service..."
cat << 'EOF' > $MOUNT_POINT/etc/systemd/system/umd-buzzer-beep1sec.service
[Unit]
Description=Set UMD Buzz-on-boot Service

[Service]
ExecStart=/root/bin/umd-buzzer-beep1sec

[Install]
WantedBy=multi-user.target vng-controller.service
EOF
cat << 'EOF' > $MOUNT_POINT/root/bin/umd-buzzer-beep1sec
#!/bin/sh +e
{
        echo 16 > /sys/class/gpio/export
        echo out > /sys/class/gpio/gpio16/direction
} || {
        echo "Pin initialisation failed, continuing"
}

echo 1 > /sys/class/gpio/gpio16/value
sleep 1
echo 0 > /sys/class/gpio/gpio16/value
echo 16 > /sys/class/gpio/unexport

EOF
chmod +x $MOUNT_POINT/root/bin/umd-buzzer-beep1sec
echo "Done"

#############################

echo "Setup umd user permissions..."
cat << 'EOF' > $MOUNT_POINT/etc/sudoers.d/umd
umd	ALL=(ALL:ALL) ALL
EOF
echo "Done"

#############################

echo "Setup app on next boot... "

cp $MOUNT_POINT/etc/rc.local $MOUNT_POINT/etc/rc.local.bak

cat << 'EOF' > $MOUNT_POINT/etc/rc.local
#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#

/root/bin/umd-buzzer-beep1sec

set -o xtrace
/root/bin/sd-unlock 									# Unlock file system
mount

_IP=$(hostname -I) 										# Print the IP address
if [ "$_IP" ]; then
  printf "My IP address is %s\n" "$_IP"
fi
ETH_MAC=$(cat /sys/class/net/eth0/address | tr -d ':' | tr '[:lower:]' '[:upper:]' | tr -d '\n')
CPU_ID=$(cat /proc/cpuinfo | grep "Serial" | awk '{ print $3 }' | tr -d '\n')
DEVICE_ID=$(cat /boot/umd_device_id.txt | tr -d '\n')

curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{
   \"localAddress\": \"$_IP\",
   \"partNumber\": \"M800AR100V1\",
   \"notes\": \"updating;device_id=${DEVICE_ID};eth_mac=${ETH_MAC};cpu_id=${CPU_ID}\"
 }" "http://192.168.1.21/api/device/notify/" 			# Notify production server of new device
echo ""

#printf "Installing/loading dependancies\n"
date -s "Mon Dec 18 06:00:00 UTC 2017" 					# update current date to a minimum date/time

# Setup the proxy to get cached apt-get update
export http_proxy=http://192.168.13.1:8000/

while fuser /var/lib/dpkg/lock /var/lib/apt/lists/lock /var/cache/apt/archives/lock >/dev/null 2>&1; do echo 'Waiting for release of dpkg/apt locks'; sleep 2; done; apt-get update

# if [ "DEV" -ne "$1" ]
# then 
# apt-get dist-upgrade -y
# fi

apt-get install -y avahi-daemon
# Required by demo/test perl script
apt-get install -y libdevice-serialport-perl

apt-get install -y jq inotify-tools ntpdate
apt-get install -o Dpkg::Options::="--force-confold" -y watchdog

# Reset the proxy settings
unset http_proxy

ntpdate pool.ntp.org									# update current date/time

groupadd watchdog
usermod -aG gpio umd									# set user permissions
usermod -aG watchdog umd

# setup/run UMD basic services 
systemctl enable umd-sethost.service					# setup hostname services
systemctl start umd-sethost.service
systemctl enable avahi-daemon.service
systemctl start avahi-daemon.service
systemctl enable umd-watchdogpermissions.service
systemctl start umd-watchdogpermissions.service
systemctl enable umd-buzzer-beep1sec.service
systemctl start umd-buzzer-beep1sec.service

chown -R umd:users /home/umd/apps						# Edit permissions
chmod +x /home/umd/apps/vng-controller/Scripts/*.sh
chmod +x /home/umd/apps/vng-controller-out/Scripts/*.sh
chmod +x /home/umd/apps/vast-local-validator/Scripts/*.sh

cp /home/umd/apps/vng-controller/Scripts/vng-controller.service /etc/systemd/system/					# copy systemd scripts
cp /home/umd/apps/vast-local-validator/Scripts/vast-local-validator.service /etc/systemd/system/
cp /home/umd/apps/vng-controller-out/Scripts/vng-controller-out.service /etc/systemd/system/			# add line for dual service

systemctl enable vast-local-validator.service  		# Enable local validator service

/home/umd/apps/vng-controller/Scripts/provision.sh E2A83674-AFAA-47D6-8497-8CAA7531C088					# Provision device on VAST using provision only

/root/bin/sd-unlock										# ensure device is still unlocked after provision

systemctl enable vng-controller.service					# Enable controller service
systemctl enable vng-controller-out.service

rm /etc/rc.local										# Restore rc.local
mv /etc/rc.local.bak /etc/rc.local
systemctl daemon-reload
chmod +x /etc/rc.local

curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{
   \"localAddress\": \"$_IP\",
   \"partNumber\": \"M800AR100V1\",
   \"notes\": \"ready;device_id=${DEVICE_ID};eth_mac=${ETH_MAC};cpu_id=${CPU_ID}\"
 }" "http://192.168.1.21/api/device/notify/" 			# Notify production server of new device
echo ""

/root/bin/umd-buzzer-beep1sec
sleep 1
/root/bin/umd-buzzer-beep1sec
shutdown -r now 										# End /etc/rc.local and reboot

exit 0


EOF
echo "Done"

if [ "DEV" = "$1" ]
then 

echo "Use vast-stage as endpoint... "
_SCRIPT=$MOUNT_POINT$APP_TARGET1/Scripts/provision.sh
sed -e "s/VAST_ENDPOINT=https:\/\/vast.umd.com.au/VAST_ENDPOINT=https:\/\/vast-stage.umd.com.au/ig" $_SCRIPT > $_SCRIPT.tmp && mv $_SCRIPT.tmp $_SCRIPT

_SCRIPT=$MOUNT_POINT$APP_TARGET2/Scripts/provision.sh
sed -e "s/VAST_ENDPOINT=https:\/\/vast.umd.com.au/VAST_ENDPOINT=https:\/\/vast-stage.umd.com.au/ig" $_SCRIPT > $_SCRIPT.tmp && mv $_SCRIPT.tmp $_SCRIPT

fi

echo "Set device to be DHCP... "
cat << 'EOF' > $MOUNT_POINT/etc/network/interfaces.d/eth0
auto eth0
iface eth0 inet dhcp
EOF

echo "Unmounting device... " 

echo "Finish"
